jQuery(document).ready(function() {
    if(typenow == "projects") {
        function start(){
            interval = setInterval(checked, 3000);
        }

        function checked() {
            console.log($('.pods-dfv-list').children().length)
            $test = $('.pods-dfv-list').children().length;
            if($test > 0 ){
                $('#publish').removeAttr('disabled');
                $( '.pods-form-ui-row-name-logo > td .pods-validate-error-message' ).last().remove()
                clearInterval(interval);
            }
        }
    }
    // publish button validation
	jQuery('#publish').click(function(){

		if(typenow == "projects"){
            $title_value = jQuery.trim(jQuery('#title').val());
            $logo		=  $('.pods-dfv-list').children().length;
            if($title_value == 0 && $title_value != " "){
                alert('Please insert title');
                jQuery('.spinner').css("visibility", "hidden");
                jQuery('#title').focus();
                return false;
            }

            if( $logo == 0 ){
                $( '.pods-form-ui-row-name-logo > td' ).last().prepend( '<div class="pods-validate-error-message">Logo is required.</div>' );
                jQuery('.spinner').css("visibility", "hidden");
                $('#publish').prop('disabled', true);
                start();
                return false;
			}
		}

	});
	// draft button validation
	jQuery('#save-post').click(function(){

		if(typenow == "projects"){
            $title_value = jQuery.trim(jQuery('#title').val());
            $logo		=  $('.pods-dfv-list').children().length;

            if($title_value == 0 && $title_value != " "){
                alert('Please insert title');
                jQuery('.spinner').css("visibility", "hidden");
                jQuery('#title').focus();
                return false;
            }

            if( $logo == 0 ){
                $( '.pods-form-ui-row-name-logo > td' ).last().prepend( '<div class="pods-validate-error-message">Logo is required.</div>' );
                jQuery('.spinner').css("visibility", "hidden");
                start();
                return false;
            }
		}

	});
});