<?php

#-------------------------- style ----------------------------------

function KVRD2_resources_style()
{

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/asset/css/bootstrap.min.css');
    wp_enqueue_style('swiper', get_template_directory_uri() . '/asset/css/swiper.min.css');
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/asset/css/fontawesome-all.min.css');
    wp_enqueue_style('reset', get_template_directory_uri() . '/asset/css/reset.css');
    wp_enqueue_style('basics', get_template_directory_uri() . '/asset/css/common.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/asset/css/style.css');
    wp_enqueue_style('flexslider', get_template_directory_uri() . '/asset/css/flexslider.css');
    wp_enqueue_style('date', get_template_directory_uri() . '/asset/css/datepicker.css');

}

add_action('wp_enqueue_scripts', 'KVRD2_resources_style');

#-------------------------SetUP-------------------------------

function KVRD_setup()
{


    //Add featured image support
    add_theme_support('post-thumbnails');
    add_image_size('small-thumbnail', 200, 300, array('left', 'top'), true); //size of image
    add_image_size('banner-image', 920, 210, array('left', 'top')); //size of image and postion
    add_image_size('video-image', 300, 250, array('left', 'top')); //size of image and postion

}

add_action('after_setup_theme', 'KVRD_setup');

add_image_size('medium', 907, 705, true);
add_image_size('small', 250, 100, true);
add_image_size('press', 1044, 470, true);


/**
 * ACF Google Map Custom Field.
 */

function my_acf_google_map_api($api)
{

    $api['key'] = 'AIzaSyCdbNGZNxXWApXH7VKPXkfE2tSqV4tYZ9o';

    return $api;

}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


#-----------------------------Menu------------------------------
function add_menu()
{
    register_nav_menus(array(
        'main' => 'Main Menu',
        'side' => 'side Menu',
    ));

}

add_action('init', 'add_menu');

function nav_menu()
{
    wp_nav_menu(array(
        'menu' => 'main',
        'container_class' => false,
    ));
}

add_filter('nav_menu_css_class', 'menu_item_classes', 10, 4);
//mainHover

function menu_item_classes($classes, $item, $args, $depth)
{

//    unset($classes);
    if ('main Menu' === $args->menu) {
        $classes[] = 'text-uppercase d-inline-block head-padding';
    }

    return $classes;
}

#-----------------------------Mail------------------------
function wpse27856_set_content_type()
{
    return "text/html";
}

add_filter('wp_mail_content_type', 'wpse27856_set_content_type');

#----------------------------------AJax newsLetters -------------------------------

add_action("wp_ajax_news_letter", "news_letter_function");
add_action("wp_ajax_nopriv_news_letter", "news_letter_function");

function news_letter_function()
{


    global $wpdb;

    $email = $email = $_POST['email'];
    $arr = [];
    $index = 0;

    $date = new DateTime();
    $created_at = $date->getTimestamp();


    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';

    } else {

        $table_name = $wpdb->prefix . "wysija_user";
        $test = $wpdb->insert($table_name, array('email' => $email, 'created_at' => $created_at, 'status' => 1));

        $sql = "INSERT INTO 'wp_wysija_user' ('email', 'created_at', 'status') VALUES ($email,  '0',  '1')";

        if ($test == false) {
            $errors[] = 'You are already subscribed!';
        }
    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
        }

    } else {
        $arr[$index]['success'] = 'Thank You!';
        $arr[$index]['error'] = '';
    }


    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($arr));

}

function add_ajaxurl_cdata_to_front()
{ ?>
    <script type="text/javascript"> //<![CDATA[
        ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        //]]> </script>
<?php }

add_action('wp_head', 'add_ajaxurl_cdata_to_front', 1);

#----------------------------------AJAX see more ------------------------------------
function more_post_ajax()
{

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 3;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    header("Content-Type: text/html");

    $args = array(
        'suppress_filters' => true,
        'post_type' => 'post',
        'posts_per_page' => $ppp,
        'post_type' => 'projects',
        'paged' => $page,
        "order" => 'ASC',
    );

    $loop = new WP_Query($args);
    $conut = $loop->post_count;
    $x = 0;
    $out = '';

    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
        $id = get_the_ID();
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full')[0];
        $logo = get_post_meta($id, 'logo')[0]['guid'];
        $excerpt = get_post_meta($id, 'project_excerpt');
        $url = get_post_permalink($id);

        $out .= '<div class="singleProject clearfix position-relative mrg-btm-lg">
                        <div class="col-md-9 image centerImg-md p-0">
                            <img src="' . $image . '" alt="">
                        </div>
                        <div class="mainColorBg commonDiv float-right">
                            <div class="projectLogo m-auto">
                                <img src="' . $logo . '" alt="">
                                <p class="aperturaRegular text-uppercase desc">
                                    ' . $excerpt[0] . '
                                </p>
                                <div class="text-center">
                                <a href="' . $url . '" class="aperturaRegular d-inline-block">MORE</a>
                                </div>
                            </div>

                        </div>
                    </div>';

    endwhile;
    endif;
    $t = true;
    if ($loop->max_num_pages - $page == 0) $t = false;
    wp_reset_postdata();
//    wp_send_json(['t' => $t,'out' => $out]);
    die(json_encode(['t' => $t, 'out' => $out]));
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

#-----------------------------------more careers ---------------------------------------
function career_post_ajax()
{

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 3;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    header("Content-Type: text/html");

    $args = array(
        'suppress_filters' => true,
        'posts_per_page' => $ppp,
        'post_type' => 'careers',
        'paged' => $page,
        "order" => 'ASC',
    );

    $loop = new WP_Query($args);
    $conut = $loop->post_count;
    $x = 0;
    $out = '';

    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
        $id = get_the_ID();
        $excerpt = get_post_meta($id, 'career_excerpt');
        $url = get_post_permalink($id);

        $out .= '<div class="col-md-6 col-xl-4">
                        <div class="singleCarer">
                            <p class="f-big2 aperturaRegular letter-4"> ' . get_the_title() . '</p>
                            <p class="aperturaRegular twoLines letter-4 explain f-normal">'. $excerpt[0] .'</p>
                            <a href="'.$url.'">
                                <button class="aperturaRegular border-0 commonButton">
                                    Apply now
                                </button>
                            </a>
                        </div>
                    </div>';

    endwhile;
    endif;
    $t = true;
    if ($loop->max_num_pages - $page == 0) $t = false;
    wp_reset_postdata();
//    wp_send_json(['t' => $t,'out' => $out]);
    die(json_encode(['t' => $t, 'out' => $out]));
}

add_action('wp_ajax_nopriv_career', 'career_post_ajax');
add_action('wp_ajax_career', 'career_post_ajax');

#----------------------------------Ajax project-email ---------------------------------
add_action("wp_ajax_message", "message_function");
add_action("wp_ajax_nopriv_message", "message_function");

function message_function()
{


    global $wpdb;

    $name = $_POST['name'];
    $email = $_POST['email'];
    $number = $_POST['number'];

    $arr = [];
    $index = 0;

    $headers = array('Content-Type: text/html; charset=UTF-8');

    $required = array($email, $number, $name);
    foreach ($required as $field) {
        if ($field == '') {
            $errors[] = 'All Fields are required.';
            break;
        }
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';

    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
            $index++;
        }

    } else {
        $to = 'info@kvrd.com.eg';
        $subject = 'KVRD';
        $full_message = "
                        Email   : $email<br/>
                        name    : $name <br/>
 						Subject : $subject<br/>
 						Number  : $number
		";

        $send_email = wp_mail($to, $subject, $full_message, $headers);

        $arr[$index]['success'] = 'Thank You';
        $arr[$index]['error'] = '';
    }


    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($arr));

}

#----------------------------------AJax Careers-------------------------------

add_action("wp_ajax_careers", "careers_function");
add_action("wp_ajax_nopriv_careers", "careers_function");

function careers_function()
{

    require_once(ABSPATH . "/vendor/anthonybudd/wp_mail/src/WP_Mail.php");


    if (!function_exists('wp_handle_upload')) {
        require_once(ABSPATH . 'wp-admin/includes/file.php');
    }

    $uploadedfile = $_FILES['file'];

    if(!empty($_FILES['file'])) {
        $file = $_FILES['file'];

        $name = $file['name'];
        $nameArray = explode('.', $name);
        $fileName = $nameArray[0];
        $fileExt = $nameArray[1];

        $mime = explode('/', $file['type']);
        $mimeType = $mime[0];
        $mimeExt = $mime[1];

        $tmpLoc = $file['tmp_name'];
        $fileSize = $file['size'];

        $allowed = array('pdf');

        if ($mimeType != 'application') {
            $errors[] = 'The File must be an PDF.';
        }
        if (!in_array($fileExt, $allowed)) {
            $errors[] = 'The File extension must be a pdf';
        }
        if ($fileSize > 2000000) {
            $errors[] = 'The File size must be under 2MB';
        }
        if ($fileExt != $mimeExt && ($mimeExt == 'pdf' & $fileExt != 'pdf')) {
            $errors[] = 'File extension does not match the file.';
        }
    }

//    if ($movefile && !isset($movefile['error'])) {
//        var_dump('success');
//    }

    $data = $_POST;

    foreach ($data as $key => $value){
        $field[$key] = $value;
    }

    $index = 0;
    $name = $field['name'];
    $email = $field['carrer_email'];
    $phone = $field['phone'];
    $military = $field['military'];

    $required = array($email, $name, $phone, $military);
    foreach ($required as $field) {
        if ($field == '') {
            $errors[] = 'All Fields are required.';
            break;
        }
    }


    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';
    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
            $index++;
        }
    } else {
        $upload_overrides = array('test_form' => false);

        $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
        $send_email = WP_Mail::init()
            ->to('eng.husseinad@gmail.com')
            ->subject('KVRD careers!')
            ->template(get_template_directory() . '/emails/template.php', [
                'data' => $data
            ]);

            if(!empty($_FILES['file'])){
                $send_email->attach($movefile['file']);
            }
            $send_email->send();


        $arr[$index]['success'] = 'Thank You';
        $arr[$index]['error'] = '';
    }

    header('Content-Type: application/json');
    die(json_encode($arr));

}

#---------------------------------Ajax property--------------------------------
add_action("wp_ajax_property", "property_function");
add_action("wp_ajax_nopriv_property", "property_function");

function property_function()
{


    global $wpdb;

    $name = $_POST['name'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $owner = $_POST['owner'];
    $project = $_POST['project'];
    $type = $_POST['type'];
    $bedroom = $_POST['bedroom'];

    if ($type == 'residential') {
        $room = 'Bedroom number: ' . $bedroom;
    }

    $arr = [];
    $index = 0;

    $headers = array('Content-Type: text/html; charset=UTF-8');
    $full_message = 'From: ' . $name . ',<br/>' . $_POST['text'];

    $required = array($email, $name, $project);
    foreach ($required as $field) {
        if ($field == '') {
            $errors[] = 'All Fields are required.';
            break;
        }
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';

    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
            $index++;
        }

    } else {
        $to = 'info@kvrd.com.eg';
        $subject = 'KVRD details';
        $full_message = "
                        Email           : $email<br/>
                        name            : $name <br/>
                        KVRD customer   : $owner  <br/>
 						Subject         : $subject<br/>
 						project         : $project<br/>
 						mobile          : $mobile<br/>
 						$room
		";

        $send_email = mail($to,$subject,$full_message,$headers);
//        $send_email = wp_mail($to, $subject, $full_message, $headers);

        $arr[$index]['success'] = 'Thank You';
        $arr[$index]['error'] = '';
    }


    header('Content-Type: application/json');
    $text = json_encode($type);
    die(json_encode($arr));

}

#----------------------------------AJax message-------------------------------

add_action("wp_ajax_message2", "message2_function");
add_action("wp_ajax_nopriv_message2", "message2_function");

function message2_function()
{


    global $wpdb;

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $number = $_POST['number'];
    $message = $_POST['message'];

    $arr = [];
    $index = 0;

    $headers = array('Content-Type: text/html; charset=UTF-8');
    $full_message = 'From: ' . $first_name . ',<br/>' . $_POST['text'];

    $required = array($email, $message, $first_name);
    foreach ($required as $field) {
        if ($field == '') {
            $errors[] = 'All Fields are required.';
            break;
        }
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';

    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
            $index++;
        }

    } else {
        $to = 'info@kvrd.com.eg';
        $subject = 'KVRD';
        $full_message = "
                        Email   : $email<br/>
                        name    : $first_name . $last_name<br/>
 						Subject : $subject<br/>
 						Message : $message<br/>
 						Number  : $number
		";

        $send_email = wp_mail($to, $subject, $full_message, $headers);

        $arr[$index]['success'] = 'Thank You';
        $arr[$index]['error'] = '';
    }


    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($arr));

}

#-------------------------------gallery----------------------------------------------

add_action("wp_ajax_gallery", "gallery_function");
add_action("wp_ajax_nopriv_gallery", "gallery_function");

function gallery_function()
{
    $gallery_id = $_POST['gallery_id'];

    $gallery_image = get_post_meta($gallery_id, 'upload');

    $data = [];
    $index = 0;

    foreach ($gallery_image as $gallery_image) {
        $url = $gallery_image["guid"];
        $title = $gallery_image["post_name"];

        $data[$index]['url'] = $url;
        $data[$index]['title'] = $title;
        $index++;
    }

    header('Content-Type: application/json');
    die(json_encode($data));

}
