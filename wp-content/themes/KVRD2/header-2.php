<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/swiper.min.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/reset.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/fontawesome-all.min.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/basics.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/main.css';?>">
    <link rel="stylesheet" href="<?= get_template_directory_uri().'/asset2/css/style.css';?>">
    <script src="<?= get_template_directory_uri().'/asset2/js/jquery-3.3.1.min.js';?>"></script>
    <script src="<?= get_template_directory_uri().'/asset2/js/bootstrap.min.js';?>"></script>
    <script src="<?= get_template_directory_uri().'/asset2/js/swiper.min.js';?>"></script>
    <script src="<?= get_template_directory_uri().'/asset2/js/main.js';?>"></script>

    <?php wp_head(); ?>
<!--    <script type="text/javascript" src="--><?//= get_template_directory_uri().'/asset/js/jquery-3.3.1.min.js';?><!--"></script>-->
<!--    <script type="text/javascript" src="--><?//= get_template_directory_uri().'/asset/js/bootstrap.min.js';?><!--"></script>-->
<!--    <script type="text/javascript" src="--><?//= get_template_directory_uri().'/asset/js/swiper.min.js';?><!--"></script>-->
    <script type="text/javascript" src="<?= get_template_directory_uri().'/asset/js/main.js';?>"></script>
    <script type="text/javascript" src="<?= get_template_directory_uri().'/asset/js/jquery.flexslider-min.js';?>"></script>
    <script type="text/javascript" src="<?= get_template_directory_uri().'/asset/js/bootstrap-datepicker.js';?>"></script>

</head>
<body>
<header>
    <?php

    $facebook = get_post_meta(21,'facebook',true);
    $instagram = get_post_meta(21,'instagram',true);
    $twitter = get_post_meta(21,'twitter',true);
    $Linkedin = get_post_meta(21,'linkedin',true);

    ?>
    <div class="top">
        <div class="myContainer clearfix">
            <a href="<?=home_url();?>">
                <img src="<?= get_template_directory_uri().'/asset/images/logo.png';?>" alt="" class="float-left logo">
            </a>
            <div class="float-right aroundSocial">

                <div class="socialIcons pr-3 toHide">
                    <a href="<?=$Linkedin?>" class="">
                        <i class="fab fa-linkedin"></i>
                    </a>
                    <a href="<?=$instagram?>" class="">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="<?=$twitter?>" >
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="<?=$facebook?>" class="">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                </div>
            <?php get_search_form(); ?>
        </div>
    </div>

    <hr>
    <div class="bottom">
        <div class="myContainer clearfix position-relative">
            <a href="javascript:(void(0))" class="float-left navIcon">
                <i class="fas fa-bars mainColor f-32"></i>
                <i class="fas fa-times f-20 mainColor mainHover closeSubMenu"></i>
            </a>
            <?php
            $args = array(
                'menu'  => 'main Menu',
                'container' => 'ul',
                'items_wrap' => '<ul class="float-left d-in-large">%3$s</ul>',

            );
            wp_nav_menu( $args );

            ?>
            <div class="chat mainColorBg float-right d-flex justify-content-center align-items-center position-absolute">
                <a href="<?= $facebook;?>">
                    <i class="fas fa-comment mr-1"></i>
                    LIVE CHAT
                </a>
            </div>
        </div>
    </div>

</header>
<div class="responsiveMenu whiteBg position-fixed">
    <div class="myContainer">
        <div class="px-4">
            <?php
            $args = array(
                'menu'  => 'side Menu',
                'container' => 'ul',
                'items_wrap' => '<ul >%3$s</ul>',

            );
            wp_nav_menu( $args );

            ?>

        </div>
    </div>
</div>
<div class="alert alert-success text-center" style="display: none; position: fixed;z-index: 100000;">
    <strong>Success!</strong> This alert box could indicate a successful or positive action.
</div>
<div id="alert_danger" class="wrongEmail text-center" style="display: none; position: fixed;z-index: 100000; min-width: 29%">

</div>
