<?php get_header('2'); ?>

<?php // Show the selected media content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>

        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset2/images/media-center.png'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
        </section>

        <?php
        $query_images_args = array(
            'post_type' => 'gallery',
            'posts_per_page' => 6,
            "order" => 'ASC',
        );
        $media = get_posts($query_images_args);
        foreach ($media as $media) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($media->ID), 'full');
            $images [] = $image[0];
            $gallery_id[] = $media->ID;

//                $gal_image  = get_post_meta(44, 'upload');
//                var_dump($gal_image);
//                die();
//
//                echo '<hr>';
        }


        ?>

        <section class="photos padded gray-bg">
            <p class="f-30 text-center mb-5 text-uppercase">Photo Gallery</p>
            <div class="images clearfix">
                <?php for ($i = 0; $i < 6; $i++) { ?>

                    <div class="img col-lg-4 col-md-6 col-12 h-lg float-left sel_cat" data-id="<?= $gallery_id[$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="0">
                    </div>
                    <div class="img col-lg-8 col-md-6 col-12 h-md float-right sel_cat"
                         data-id="<?= $gallery_id[++$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="1">
                    </div>
                    <div class="img col-lg-4 col-md-6 col-12 h-lg float-right sel_cat"
                         data-id="<?= $gallery_id[++$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="3">
                    </div>
                    <div class="img col-lg-4 col-md-6 col-12 h-sm sel_cat" data-id="<?= $gallery_id[++$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="2">
                    </div>
                    <div class="img col-lg-4 col-md-6 col-12 h-md float-left sel_cat"
                         data-id="<?= $gallery_id[++$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="4">
                    </div>
                    <div class="img col-lg-4 col-md-6 col-12 h-md sel_cat" data-id="<?= $gallery_id[++$i]; ?>">
                        <img src="<?= $images[$i]; ?>" data-index="5">
                    </div>

                <?php } ?>
            </div>

            <div class="pop-up f-30 commonPop">
                <div class="swiper-container photo-slider">
                    <a href="javascript:void(0)" class="float-right">
                        <i class="fa fa-bars f-22 fa-times white closeMyPopup"></i>
                    </a>
                    <div class="swiper-wrapper swiper-photo">


                    </div>

                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>

                </div>
            </div>
        </section>

        <?php

        $query_video_args = array(
            'post_type' => 'test',
            'posts_per_page' => -1,
        );
        $video_media = get_posts($query_video_args);

        ?>

        <section class="videos padded white-bg">
            <p class="f-30 text-center mb-5 text-uppercase">Video Gallery</p>
            <div class="row">
                <?php foreach ($video_media as $videos) {
                    $id = $videos->ID;
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'video-image');
                    $video = get_post_meta($id, 'media');

                    ?>
                    <div class="video col-lg-3 col-md-6 col-12 position-relative">
                        <div class="img-centered">
                            <img src="<?= $image[0]; ?>">
                            <div class="position-absolute play white">
                                <i class="far fa-play-circle" data-url="<?= $video[0]['guid']; ?>"></i>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="pop-up commonPop">
                <a href="javascript:void(0)" class="float-right">
                    <i class="fa fa-bars f-22 fa-times white closeMyPopup"></i>
                </a>
                <div class="h-100 d-flex justify-content-center mt-5">
                    <video mute class="myVideo" id="myVideo">
                        <source src="images/small.webm" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
            </div>
        </section>

        <?php

        $query_press_args = array(
            'post_type' => 'press',
            'posts_per_page' => -1,
        );
        $press = get_posts($query_press_args);


        ?>

        <section class="press-release gray-bg padded">
            <p class="f-30 text-center mb-5 text-uppercase">Press Releases</p>
            <div class="swiper-container press-slider">
                <div class="swiper-wrapper">
                    <?php foreach ($press as $press) {

                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($press->ID), 'full');
                        $no_of_photo = sizeof(get_post_meta($press->ID, 'gallery'));
                        $date = get_the_date('F Y', $press->ID);
                        ?>
                        <div class="swiper-slide">
                            <div class="img-centered">
                                <img src="<?= $image[0] ?>">
                                <div class="hover">
                                    <p class="f-18 text-uppercase"><?= $press->post_title; ?></p>
                                    <p>in <?= $date; ?> - <?= $no_of_photo; ?> Photos</p>
                                    <p><?= $press->post_excerpt; ?> <a
                                                href="<?= get_post_permalink($press->ID) ?>"><span class="lightColor">Read more</span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="arrows d-flex justify-content-between">
                <div class="swiper-button-prev"><i class="fa fa-long-arrow-alt-left"></i></div>
                <div class="swiper-button-next"><i class="fa fa-long-arrow-alt-right"></i></div>
            </div>
        </section>
    <?php
    endwhile;
endif;
?>
    <script type="text/javascript">
        $(function () {
            $('.sel_cat').click(function () {

                var gallery_id = $(this).attr("data-id");
                console.log(gallery_id);
                $(".swiper-photo").empty();

                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: ajaxurl,
                    cache: false,
                    data: {"action": "gallery", gallery_id: gallery_id},
                    success: function (data) {
                        console.log(data);

                        loop(data);
                        var photoSwiper = new Swiper('.photo-slider', {
                            loop: true,
                            spaceBetween: 100,
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev'
                            }
                        });
                    }
                });

                function loop(data) {
                    var all = data;

                    jQuery.each(all, function (i, val) {
                        console.log(val);
                        $(".swiper-photo").prepend(
                            '<div class="swiper-slide swiper-slide-active row justify-content-center"><div class="col-lg-4 img-centered"> <img src=" ' + val.url + ' "> </div><div class="text col-lg-4"><p>Title</p><p>' + val.title + '</p></div></div>')
                    });
                }

            });
        });
    </script>


<?php get_footer(); ?>