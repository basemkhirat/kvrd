<?php get_header('2');?>

<?php // Show the selected Mission content.
if ( have_posts() ) :
    while ( have_posts() ) : the_post();
?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset2/images/mission-1.jpg'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg commonDiv">
                    <h1 class="white letter-4 text-uppercase"><?= get_post_meta($post->ID, 'title1', true); ?></h1>
                    <div class="smallHr"></div>
                    <div class="row">
                        <p class="f-normal white desc letter-4 twoLines col-10"><?= get_post_meta($post->ID, 'description1', true); ?></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="white-bg">
        <div class="img-text d-lg-flex justify-content-center">
            <div class="img img-centered col-lg-8 mg-right-minus large">
                <img src="<?= get_template_directory_uri().'/asset2/images/mission-vision-1.jpg'?>">
            </div>
            <div class="text col-lg-5 align-self-center height-360 on-top">
                <p class="text-uppercase text-center title large white"><?= get_post_meta($post->ID, 'title2', true); ?></p>
                <p class="f-22"><?= get_post_meta($post->ID, 'description2', true); ?></p>
            </div>
        </div>
    </section>
<?php
    endwhile;
endif; ?>

<?php get_footer(); ?>