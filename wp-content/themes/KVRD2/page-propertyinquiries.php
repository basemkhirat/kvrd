<?php get_header('2'); ?>

<?php // Show the selected property content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset2/images/property.jpg'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg commonDiv">
                    <h1 class="white letter-4">PROPERTY
                        INQUIRIES</h1>
                </div>
            </div>
        </section>

        <section class="property-2">
            <div class="myContainer">
                <p>PLEASE WRITE YOUR DETAILS HERE:</p>

                <form action="" id="prop_form">
                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="name" class="f-22 col-md-4 p-0">Full Name :</label>
                            <input id="name" maxlength="30" type="text" class="form-control f-input col-md-8" id="name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="email" class="f-22 col-md-4 p-0">Email:</label>
                            <input id="prop_email" maxlength="50" type="email" class="form-control f-input col-md-8" id="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="mobile" class="f-22 col-md-4 p-0">Mobile :</label>
                            <input id="mobile" min="1" max="5" maxlength="15" type="text" class="form-control f-input col-md-8" id="mobile"
                                   required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="owner" class="f-22 col-md-4 p-0">KVRD Customer : </label>
                            <i class="fas fa-angle-down"></i>
                            <select name="" id="owner" class="form-control f-input col-md-8">
                                <option value="no">No</option>
                                <option value="yes">Yes</option>
                            </select>
                        </div>
                    </div>
                    <?php
                    $project_args = array(
                        'post_type' => 'projects',
                        "order" => 'ASC',
                    );
                    $projects = get_posts($project_args);


                    ?>
                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="project" class="f-22 col-md-4 p-0">Projects of Interest : </label>
                            <i class="fas fa-angle-down"></i>
                            <select name="" id="project" class="form-control f-input col-md-8">
                                <option value="">Select one of the projects below</option>
                                <?php foreach ($projects as $project){
                                    $term_id = wp_get_post_terms( $project->ID, array( 'project-type'))[0]->term_id;
                                    ?>

                                    <option term="<?= $term_id?>" value="<?=$project->post_title;?>"><?=$project->post_title;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row position-relative">
                            <label for="type" class="f-22 col-md-4 p-0">Type :</label>
                            <i class="fas fa-angle-down"></i>
                            <select name="" id="type" class="form-control f-input col-md-8">
                                <option value="">Select Type</option>

                            </select>
                        </div>
                    </div>

                    <div id="show-bedroom" class="form-group" style="display: none">
                        <div class="row position-relative">
                            <label for="bedroom" class="f-22 col-md-4 p-0">Bedroom Number :</label>
                            <input type="text" id="bedroom" class="form-control f-input col-md-8">
                        </div>
                    </div>

                    <button id="prop_submit" type="submit" class="aperturaMedium">
                        SUBMIT
                    </button>
                </form>
            </div>
        </section>
<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>-->
        <script type="text/javascript">

            $('#project').change(function () {
                var type = $('#project').find(":selected").attr("term");
                $('#type').empty();
                if(type == 6){
                    // Offices, Clininics, Retail Space
                    $('#type').append('<option value="">Select Type</option>\n');
                    $('#type').append('<option value="Offices">Offices</option>');
                    $('#type').append('<option value="Clininics">Clininics</option>');
                    $('#type').append('<option value="Retail Space">Retail Space</option>');
                    $('#show-bedroom').css("display","none");
                }
                else if(type == 7){
                    // apartment, villa, twin house, town house, duplex, studio
                    $('#type').append('<option value="">Select Type</option>\n');
                    $('#type').append('<option value="apartment">apartment</option>');
                    $('#type').append('<option value="villa">villa</option>');
                    $('#type').append('<option value="twin house">twin house</option>');
                    $('#type').append('<option value="town house">town house</option>');
                    $('#type').append('<option value="duplex">duplex</option>');
                    $('#type').append('<option value="studio">studio</option>');
                    $('#show-bedroom').css("display","block");
                }
            });

            $(function () {

                $('#prop_form').submit(function (e) {
                    e.preventDefault();
                    var name        = $("#name").val();
                    var email       = $("#prop_email").val();
                    var mobile      = $("#mobile").val();
                    var owner       = $("#owner").val();
                    var project     = $("#project").val();
                    var type        = $("#type").val();
                    var bedroom     = $("#bedroom").val();

                    var errors = [];

                    if (email == '') {
                        errors.push("email its required");
                        $("#prop_email").parent().find('i').remove();
                        $("#prop_email").css("border","1px solid #d62222" );
                        $("#prop_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    }else {
                        var atpos = email.indexOf("@");
                        var dotpos = email.lastIndexOf(".");
                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
                            errors.push('Not a valid e-mail address!');
                            $("#prop_email").parent().find('i').remove();
                            $("#prop_email").css("border","1px solid #d62222" );
                            $("#prop_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                        }
                    }


                    if (mobile == '') {
                        errors.push('number its required!');
                        $("#mobile").parent().find('i').remove();
                        $("#mobile").css("border","1px solid #d62222" );
                        $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    } else {

                        if(isNaN(mobile) == true){
                            errors.push('invalid number only contain number!');
                            $("#mobile").parent().find('i').remove();
                            $("#mobile").css("border","1px solid #d62222" );
                            $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                        }else{
                            if (mobile.length < 8 || mobile.length > 15) {
                                errors.push('valid number must be between 8 and 15 numbers !');
                                $("#mobile").parent().find('i').remove();
                                $("#mobile").css("border","1px solid #d62222" );
                                $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                            }
                        }

                    }

                    if (errors != '') {
                        $('#alert_danger').empty();

                        for (i = 0; i < errors.length; i++) {
                            // console.log(errors[i]);

                            $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                            $("#danger" + i).append('<strong>Error!</strong> ' + errors[i]);

                        }

                        $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                            $("#alert_danger").slideUp(9000);
                        });
                    }

                    if (errors == '') {

                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: ajaxurl,
                            cache: false,
                            data: {
                                "action": "property",
                                email: email,
                                name: name,
                                mobile: mobile,
                                owner: owner,
                                project: project,
                                type: type,
                                bedroom: bedroom
                            },
                            success: function (data) {
                                // console.log(data);
                                error = data[0].error;
                                success = data[0].success;

                                if (data[0].error != '') {
                                    $('#alert_danger').empty();

                                    for (i = 0; i < data.length; i++) {
                                        // console.log(data[i].error);

                                        $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                                        $("#danger" + i).append('<strong>Error!</strong> ' + data[i].error);

                                    }

                                    $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $("#alert_danger").slideUp(9000);
                                    });


                                } else {

                                    $(".alert-success").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $(".alert-success").slideUp(9000);
                                    });
                                    $(".alert").html('<strong>Success!</strong> ' + success);

                                    $("#name").val('');
                                    $("#prop_email").val('');
                                    $("#mobile").val('');
                                    $("#owner").val('');
                                    $("#project").val('');
                                    $("#type").val('');
                                    $("#bedroom").val('');

                                }
                            }
                        });
                    }
                });


            });
        </script>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>