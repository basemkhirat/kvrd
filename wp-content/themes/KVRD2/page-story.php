<?php get_header('2'); ?>

<?php // Show the selected storypage content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed app">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset2/images/our-story.jpg'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg commonDiv">
                    <h1 class="white letter-4 text-uppercase">our story</h1>
                    <div class="smallHr"></div>
                    <div class="row">
                        <p class="f-normal white desc letter-4 twoLines col-10">
                            <?= get_post_meta($post->ID, 'title1', true); ?>
                            <br>
                            <?= get_post_meta($post->ID, 'description1', true); ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $img_id = get_post_meta($post->ID, 'photo1')[0];
        $back_image1 = wp_get_attachment_url($img_id);
        ?>
        <section class="gray-bg">
            <div class="img-text d-lg-flex justify-content-center">
                <div class="img img-centered align-self-center height-360 col-lg-5 on-top mg-right-minus">
                    <img src="<?=$back_image1;?>">
                </div>
                <div class="text col-lg-7">
                    <p class="text-uppercase title f-28">What <br> makes us unique?</p>
                    <p class="f-18"><?= get_post_meta($post->ID, 'description2', true); ?></p>
                </div>
            </div>
        </section>
        <?php
        $img_id2 = get_post_meta($post->ID, 'photo_2')[0];
        $back_image2 = wp_get_attachment_url($img_id2);
        ?>
        <section
                style="background-image: url('<?= $back_image2 ?>'); background-position: center; background-size: cover">
            <div class="img-text d-lg-flex">
                <div class="text col-lg-5">
                    <p class="text-uppercase title f-28 m-b-20">YOUR STRATEGIC PARTNER IN GROWTH</p>
                    <p class="f-18"><?= get_post_meta($post->ID, 'description3', true); ?></p>
                </div>
            </div>
        </section>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>