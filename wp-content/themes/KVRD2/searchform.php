
    <form action="<?=home_url('/'); ?>" method="get" class="position-relative searchForm">
        <input type="text" name="s" placeholder="Search" required>
        <button class="position-absolute">
            <i class="fas fa-search mainColor"></i>
        </button>
    </form>
    <a href="javascript:void(0)" class="mainColor mainHover searchIcon toHide pl-3">
        <i class="fas fa-search"></i>
    </a>
</div>