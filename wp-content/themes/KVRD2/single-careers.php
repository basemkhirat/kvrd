<?php get_header();
if (have_posts()):
    $id = get_the_ID();
?>

    <section
            class="ourProject app forFixed">
        <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover" class="firstSection mb-0">
        </div>
        <div class="myContainer">
            <div class="mainColorBg commonDiv">
                <h1 class="white letter-4">CAREERS</h1>
                <div class="smallHr"></div>
                <div class="row">
                    <p class="white desc letter-4 twoLines col-12 col-md-7 f-normal">
                        It is a long established fact that a reader will be distracted by the readable content of a page when
                        looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                        of letters, as opposed to using ‘Content here, content here’, making it look like readable English. Many
                        desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a
                        search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have
                        evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="p-ver-40 pb-0">
        <div class="myContainer">
            <p class="mainColor f-lg letter-4">
                APPLICATION FOR
            </p>
            <p class="mainColor f-lg letter-4">
                EMPLOYMENT
            </p>
            <div class="smallHr mainColorBg"></div>
        </div>
        <form id="careers_Form"  method="post"  class="applicationForm" enctype="multipart/form-data">
            <div class="personalInfo">
                <div class="myContainer">
                    <p class="aperturaRegular f-big mainColor">Personal Information</p>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="name" maxlength="20" id="name" type="text" placeholder="NAME" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="address" maxlength="50" type="text" placeholder="ADDRESS" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <select name="city" class="form-control">
                                    <option>Cairo</option>
                                    <option>City</option>
                                    <option>City</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <select name="state" class="form-control">
                                    <option>State</option>
                                    <option>State</option>
                                    <option>State</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="zip" maxlength="10" type="text" placeholder="ZIP" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="phone" maxlength="15" id="phone" type="text" placeholder="PHONE NUMBER" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="military" maxlength="10" type="text" placeholder="MILITARY STATUS" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="carrer_email" maxlength="50" id="carrer_email" type="email" placeholder="EMAIL" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="position grayBg">
                <div class="myContainer">
                    <p class="aperturaRegular f-big mainColor">Position</p>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <select name="position" class="form-control">
                                    <option><?=the_title();?></option>
                                </select>
                            </div>
                        </div>
                        <!-- date -->
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="start_date" type="text" maxlength="0" class="span2 form-control todate" data-date-format="mm/dd/yy" placeholder="AVAILABLE START DATE">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="pay" type="text" maxlength="10" placeholder="DESIRED PAY" class="form-control">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="clearfix mt-4">
                                <span class="mainColor aperturaBold f-12 mb-4 float-left">EMPLOYMENT DESIRED</span>
                                <div class="float-left row justify-content-lg-center col-lg-10">
                                    <label class="aperturaRegular position-relative mainColor f-12 col-6 col-lg-3">
                                        Full Time
                                        <input type="radio" class="radio" name="optradio" value="Full Time">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="aperturaRegular position-relative mainColor f-12 col-6 col-lg-3">
                                        Part Time
                                        <input type="radio" name="optradio" class="radio" value="Part Time">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="aperturaRegular position-relative mainColor f-12 col-10 col-lg-3">
                                        Seasonal / Temporary
                                        <input type="radio" name="optradio" class="radio" value="Seasonal / Temporary">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="education">
                <div class="myContainer">
                    <p class="aperturaRegular f-big mainColor">Education</p>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="school" type="text" maxlength="30" placeholder="SCHOOL NAME" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="location" type="text" maxlength="30" placeholder="LOCATION" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="school_year" type="text" maxlength="10" placeholder="YOUR SATTENDED" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="degree" type="text" maxlength="30" placeholder="DEGREE RECEIVED" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <input name="major" type="text" maxlength="20" placeholder="MAJOR" class="form-control">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="position grayBg">
                <div class="myContainer">
                    <p class="aperturaRegular f-big mainColor">Employment History</p>
                    <div class="toAppend">
                        <div class="singleEmployee">
                            <span class="f-18 aperturaBold mb-4 d-inline-block mainColor">EMPLOYER 1</span>
                            <div class="row">

                                <div class="col-md-6 col-lg-4">

                                    <div class="form-group">
                                        <input type="text" placeholder="JOB TITLE" maxlength="50" class="form-control" name="jobtitle1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="DATES EMPLOYED" class="form-control todate" maxlength="0" data-date-format="mm/dd/yy" name="datesemployed1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="WORK PHONE" maxlength="15" class="form-control" name="workphone1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="STARTING PAY RATE" maxlength="10" class="form-control" name="startingpayrate1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" maxlength="10" placeholder="ENDING PAY RATE" class="form-control" name="endingpayrate1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="ADDRESS" maxlength="30" class="form-control" name="address1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="CITY" maxlength="10" class="form-control" name="city1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="STATE" maxlength="20" class="form-control" name="state1">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" placeholder="ZIP" class="form-control" maxlength="10" name="zip1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="mainColorBg aperturaRegular white border-0 col-md-4 historyBtn singleBtn">
                        New Employer Experience <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="portfolio">
                <div class="myContainer">
                    <div class="row m-0">

                        <div class="upload-btn-wrapper col-md-4 p-0">
                            <button class="btn">Add Portfolio <i class="fas fa-plus"></i></button>
                            <input type="file" name="myfile" id="myfile" />
                        </div>
                        <div class="col-md-8 letter-4 p-0">
                            <p class="mainColor">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="position grayBg">
                <div class="myContainer text-center">
                    <button type="submit" id="career_submit" class="mainColorBg white border-0">Submit</button>
<!--                    <input type="submit" class="mainColorBg white border-0" value="submit">-->
                </div>
            </div>

        </form>
    </section>

<?php
endif;
?>
    <script type="text/javascript">
        $(function() {

            $('#careers_Form').submit(function (e) {
                e.preventDefault();

                var datastring = $("#careers_Form").serializeArray();
                var files       = $('#myfile').val();
                var name        = $( "#name" ).val();
                var email       = $( "#carrer_email" ).val();
                var number      = $( "#phone" ).val();


                var errors = [];

                if (email == '') {
                    errors.push("email its required");
                    $("#carrer_email").parent().find('i').remove();
                    $("#carrer_email").css("border","1px solid #d62222" );
                    $("#carrer_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                }else {
                    $("#carrer_email").parent().find('i').remove();
                    var atpos = email.indexOf("@");
                    var dotpos = email.lastIndexOf(".");
                    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
                        errors.push('Not a valid e-mail address!');
                        $("#carrer_email").parent().find('i').remove();
                        $("#carrer_email").css("border","1px solid #d62222" );
                        $("#carrer_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                        console.log($("#carrer_email").parent());
                    }else {
                        $("#carrer_email").css("border","1px solid #1EB52A" );
                        $("#carrer_email").parent().find('i').remove();
                        $("#carrer_email").parent().append("<i class='fas fa-check-circle' style='font-size:24px;color:green'></i>");
                    }
                }

                if(number == ''){
                    errors.push('number its required!');
                    $("#phone").parent().find('i').remove();
                    $("#phone").css("border","1px solid #d62222" );
                    $("#phone").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                }else{
                    $("#phone").parent().find('i').remove();
                    if(number.length < 8 || number.length >15){
                        errors.push('Invalid number format!');
                        $("#phone").parent().find('i').remove();
                        $("#phone").css("border","1px solid #d62222" );
                        $("#phone").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    }else if(isNaN(number) == true){
                        errors.push('invalid number only contain number!');
                        $("#phone").parent().find('i').remove();
                        $("#phone").css("border","1px solid #d62222" );
                        $("#phone").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    }
                    else {
                        $("#phone").css("border","1px solid #1EB52A" );
                        $("#phone").parent().find('i').remove();
                        $("#phone").parent().append("<i class='fas fa-check-circle' style='font-size:24px;color:green'></i>");
                    }
                }

                if(name == ''){
                    errors.push("please type your name");
                    $("#name").parent().find('i').remove();
                    $("#name").css("border","1px solid #d62222" );
                    $("#name").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                }else {
                    $("#name").css("border","1px solid #1EB52A" );
                    $("#name").parent().find('i').remove();
                    $("#name").parent().append("<i class='fas fa-check-circle' style='font-size:24px;color:green'></i>");
                }

                if(errors != ''){
                    $('#alert_danger').empty();

                    for (i = 0; i < errors.length; i++) {
                        // console.log(errors[i]);

                        $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                        $("#danger" + i).append('<strong>Error!</strong> ' + errors[i]);

                    }

                    $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                        $("#alert_danger").slideUp(9000);
                    });
                }

                // console.log(errors);
                if(errors == '') {
                    var file_data = jQuery('#myfile').prop('files')[0];
                    var myform = document.getElementById("careers_Form");
                    var form_data = new FormData(myform);
                    form_data.append('file', file_data);
                    form_data.append('action', 'careers');
                    // form_data.append('datastring', datastring);

                    console.log(datastring);
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: ajaxurl,
                        cache: false,
                        // data: {"action": "careers", datastring: datastring},
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function (data) {
                            // console.log(data);
                            error = data[0].error;
                            success = data[0].success;

                            if (data[0].error != '') {
                                $('#alert_danger').empty();

                                for (i = 0; i < data.length; i++) {
                                    // console.log(data[i].error);

                                    $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                                    $("#danger" + i).append('<strong>Error!</strong> ' + data[i].error);

                                }

                                $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                                    $("#alert_danger").slideUp(9000);
                                });


                            } else {
                                $('.form-group i').remove();
                                $('.form-group input').css('border', '1px solid rgba(0, 0, 0, 0.1)');
                                $(".alert-success").fadeTo(9000, 9000).slideUp(9000, function () {
                                    $(".alert-success").slideUp(9000);
                                });
                                $(".alert").html('<strong>Success!</strong> ' + success);
                                $('input').val('');
                            }
                        }
                    });
                }
            });


        });
        $('.todate').datepicker();
    </script>
    <style>
        @media screen and (min-width: 768px) {
            .upload-btn-wrapper {
                max-width: 400px;
            }
        }
            .upload-btn-wrapper {
            position: relative;
            overflow: hidden;
            display: inline-block;
        }

        .btn {
            color: #fff;
            letter-spacing: 3px;
            font-family: 'aperturaRegular';
            background-color: #163B48;
            padding: 8px 20px;
            border-radius: 0;
            height: 55px;
            display: inline-block;
            font-size: 14px;
            width: 100%;
        }
        .upload-btn-wrapper input[type=file] {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
        }
    </style>

<?php get_footer(); ?>