<?php get_header('2');
if (have_posts()):
    while (have_posts()) : the_post();
        $id = get_the_ID();
        for ($i = 0; $i < sizeof(get_post_meta($id, 'gallery')); $i++) {

            $images [] = get_post_meta($id, 'gallery')[$i]['guid'];
            $lg_img[] = pods_image_url($images [$i], 'press', 0, '', true);
        }

        $no_of_photo = sizeof(get_post_meta($id, 'gallery'));
        $date = get_the_date('F Y', $press->ID);

        $back_image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full')[0];

        $id = get_the_author_meta('ID');
        ?>

        <section class="ourProject forFixed">
            <div style="background-image: url('<?= $back_image; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg commonDiv">
                    <h1 class="white letter-4 text-uppercase"><?php the_title(); ?></h1>
                    <div class="smallHr"></div>
                    <div class="row">
                        <p class="f-normal white desc letter-4 twoLines col-10">
                            in <?= $date; ?> - <?= $no_of_photo; ?> Photos
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="article2">
            <div class="myContainer">
                <div class="text-center text-md-left">
                    <?= get_avatar($id, 128, null, null, null); ?>
                </div>
                <p class="text-uppercase mainText color mrg  text-center text-md-left"><?= get_the_author_meta('nickname') ?> </p>
                <p class="text-uppercase mainText"><?php the_title(); ?></p>
                <div><?php the_content(); ?></div>

                <div class="sliderContainer d-flex align-items-center">
                    <div class="swiper-container new">
                        <div class="swiper-wrapper">

                            <?php
                            foreach ($lg_img as $lg_image) {
                                ?>
                                <div class="swiper-slide knowledge">
                                    <img src="<?= $lg_image ?>" alt="">
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>

                    </div>
                </div>
            </div>
        </section>


    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>