<?php get_header('2') ?>
    <section class="notFound background position-relative" id="firstSection"
             style="background: url('<?= get_template_directory_uri() . '/asset/images/404.png'; ?>'); background-position: top; background-size: cover">
        <p class="main mainColor">
            404
        </p>
        <p class="sub after f-big mainColor">Page Not Found</p>
        <p class="f-normal mainColor desc">Sorry, but the page you were looking for was not found. Try checking the URL
            for
            the error, then press the refresh button in your browser or try to find something else in our site.</p>

        <a href="<?= site_url()?>" class="mainColor text-center">Back to home</a>
    </section>

<?php get_footer(); ?>