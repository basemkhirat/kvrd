<?php get_header(); ?>

<?php // Show the selected project content.
if (have_posts()) :
    $name = get_queried_object()->name;
    $terms = get_terms('project-type');
    $term_id = get_queried_object()->term_id;
    ?>

    <section
            class="ourProject forFixed">
        <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover" class="firstSection mb-0">

        </div>
        <div class="myContainer">
            <div class="mainColorBg pageTitle justify-content-center">
                <span class="white letter-4 f-big text-center"><?= $name; ?> Projects</span>
            </div>
        </div>
    </section>


    <section class="allProjects mrg-btm-lg">
        <div class="myContainer">

            <?php

            $project_args = array(
                'post_type' => 'projects',
                "order" => 'ASC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'project-type',
                        'terms' => $term_id
                    )
                )
            );
            $projects = get_posts($project_args);

            foreach ($projects as $project) {
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($project->ID), 'full')[0];
                $logo = get_post_meta($project->ID, 'logo')[0]['guid'];
                $excerpt = get_post_meta($project->ID, 'project_excerpt');
                ?>
                <div class="singleProject clearfix position-relative mrg-btm-lg">
                    <div class="col-md-12 image centerImg-md p-0">
                        <img src="<?= $image; ?>" alt="">
                    </div>
                    <div class="mainColorBg version4">
                        <div class="projectLogo m-auto">
                            <a href="<?= get_post_permalink($project->ID) ?>"
                               class="aperturaRegular d-inline-block"><img src="<?= $logo; ?>" alt=""></a>
                        </div>

                    </div>
                </div>
            <?php } ?>

        </div>

    </section>

<?php

endif;

?>


<?php get_footer(); ?>
