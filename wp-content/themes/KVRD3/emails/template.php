<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
        * {
            margin: 0;
            box-sizing: border-box;
        }

        .f-30 {
            font-size: 30px;
        }

        .f-28 {
            font-size: 28px;
        }

        .myContainer {
            width: 90%;
            margin: auto;
        }

        .carrer2 {
            padding: 70px 0;
        }

        .carrer2 .formTitle {
            color: #686b6f;
            margin-bottom: 35px;
        }

        .carrer2 .tableHeader {
            background-color: #2e5395;
            height: 60px;
            box-shadow: 0 5px #000;
            padding-top: 20px;
            color: #fff;
            padding-left: 10px;
            margin-bottom: 5px;
        }

        td {
            border-top: none !important;
        }

        td:not(:last-child) {
            border-right: 1px solid #000;
        }

        tr {
            box-shadow: 0 1px #000;
        }
    </style>
</head>
<body>
<?php
foreach ($data as $key => $value){
    $field[$key] = $value;
}

$x = 1;
$arr = array();
foreach($data as $key => $dat){
    $arr[]= $key;
}

for($i =17; $i<= sizeof($arr)-2; $i++){
    $exp[$x][$arr[$i++]] = $field['jobtitle'.$x];
    $exp[$x][$arr[$i++]] = $field['datesemployed'.$x];
    $exp[$x][$arr[$i++]] = $field['workphone'.$x];
    $exp[$x][$arr[$i++]] = $field['startingpayrate'.$x];
    $exp[$x][$arr[$i++]] = $field['address'.$x];
    $exp[$x][$arr[$i++]] = $field['address'.$x];
    $exp[$x][$arr[$i++]] = $field['city'.$x];
    $exp[$x][$arr[$i++]] = $field['state'.$x];
    $exp[$x][$arr[$i]] = $field['zip'.$x];

    $x++;
}

?>
<section class="carrer2">
    <div class="myContainer">
        <p class="f-30 formTitle">Application for employment</p>

        <!-- first table -->
        <p class="tableHeader f-28">
            Personal Information
        </p>
        <table class="table" width="100%" rules="all">
            <tr>
                <td>
                    <?=$field['name'];?>
                </td>
            </tr>
            <tr>
                <td>
                    <?=$field['address'];?>
                </td>
                <td>
                    <?=$field['city'];?>
                </td>
                <td class="border-right-0">
                    <?=$field['state'];?>
                </td>
                <td>
                    <?=$field['zip'];?>
                </td>
            </tr>
            <tr>
                <td>
                    <?=$field['phone'];?>
                </td>
                <td>
                    <?=$field['carrer_email'];?>
                </td>
            </tr>
            <tr>
                <td>
                    <?=$field['military'];?>
                </td>
            </tr>
        </table>
        <!-- end of first table -->

        <!-- second Table -->
        <p class="tableHeader f-28">
            Position
        </p>
        <table class="table" width="100%" rules="all">
            <tr>
                <td>
                    <?=$field['position'];?>
                </td>
                <td colspan="2" class="border-right-0">
                    <?=$field['start_date'];?>
                </td>
                <td>
                    <?=$field['pay'];?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Employment desired :
                    <span><?=$field['optradio'];?></span>
                </td>
            </tr>
        </table>
        <!-- end of second Table -->

        <!-- third Table -->
        <p class="tableHeader f-28">
            Education
        </p>
        <table class="table" width="100%" rules="all">
            <tr>
                <td>
                    <?=$field['school'];?>
                </td>
                <td>
                    <?=$field['location'];?>
                </td>
                <td>
                    <?=$field['school_year'];?>
                </td>
                <td>
                    <?=$field['degree'];?>
                </td>
                <td>
                    <?=$field['major'];?>
                </td>
            </tr>

        </table>
        <!-- end of third Table -->

        <!-- fourth table -->
        <p class="tableHeader f-28">
            Employment History
        </p>


        <table class="table" width="100%" rules="all">
            <?php
            $z=1;

            foreach ($exp as $each_exp){
                $job_title        = $each_exp['jobtitle'.$z];
                $dates_emp        = $each_exp['datesemployed'.$z];
                $phone_emp        = $each_exp['workphone'.$z];
                $start_pay_emp    = $each_exp['startingpayrate'.$z];
                $end_pay_emp      = $each_exp['endingpayrate'.$z];
                $address_emp      = $each_exp['address'.$z];
                $city_emp         = $each_exp['city'.$z];
                $state_emp        = $each_exp['state'.$z];
                $zip_emp          = $each_exp['zip'.$z];
                ?>

                <tr>
                    <td style="color: #0A246A; font-weight: 900; text-transform: uppercase;">
                        Employer <?=$z;?>
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold"><?=(($job_title != '')? $job_title : 'job title' )?> </td>
                    <td colspan="2"><?=(($dates_emp != '')? $dates_emp : 'date' )?></td>
                    <td><?=(($phone_emp != '')?$phone_emp:'phone');?></td>
                </tr>
                <tr>
                    <td><?=(($start_pay_emp != '')?$start_pay_emp:'start pay rate');?></td>
                    <td colspan="2"><?=(($end_pay_emp != '')?$end_pay_emp:'ending pay rate');?></td>
                    <td><?=(($address_emp != '')?$address_emp:'Address');?></td>
                </tr>
                <tr>
                    <td><?=(($city_emp != '')?$city_emp:'city');?></td>
                    <td><?=(($state_emp != '')?$state_emp:'state');?></td>
                    <td><?=(($zip_emp != '')?$zip_emp:'Zip');?></td>
                </tr>
                <?php $z++; }?>

    
        </table>
        <!-- end of fourth table -->
    </div>
</section>
</body>
</html>