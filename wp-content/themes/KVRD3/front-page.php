<?php get_header();

if (have_posts()) :
    while (have_posts()) : the_post();

        $id = get_post_meta($post->ID, 'upload')[0];
        $det = get_post_mime_type($id);
        $mime = explode('/', $det);
        $link = get_post_meta($post->ID, 'link')[0];
        $url = wp_get_attachment_url($id);
        $page_id = get_the_ID();
        $muted = get_post_meta($post->ID, 'muted')[0][0];

        if ($muted == 'muted') {
            $muted = 'muted';
            $y_muted = '&mute=0';
        }

        $img_id = get_post_meta($post->ID, 'back-ground')[0];
        $back_image = wp_get_attachment_url($img_id);

        ?>
        <!--start video section-->
        <section class="home-1 position-relative forFixed">

            <?php
            if ($link != '') {
                ?>

                <embed src="<?= $link; ?>?autoplay=1&amp;hl=en_US&amp;rel=0&amp;autohide=1&amp;controls=1&amp;showinfo=0&loop=1&amp;<?= $y_muted; ?>"
                       wmode="transparent" width="100%" height="500px"
                       allowfullscreen="true" type="video/webm" title="Adobe Flash Player">

            <?php } else {
                ?>
                <video <?= $muted; ?> autoplay class="myVideo" id="myVideo" poster="<?= $back_image ?>" >
                    <source src="<?= $url; ?>" type="video/mp4">
                    Your browser does not support HTML5 video.
                </video>
                <div class="mute position-absolute" style="right: 30px; bottom: 30px; font-size: 26px">
                    <a href="javascript:void(0)" id="mute">
                        <i class="fas fa-volume-up"></i>
                    </a>
                </div>
            <?php } ?>
<!--            <img src="--><?//= $back_image ?><!--" alt="" class="forVideo">-->

            <div class="position-absolute play myPlay">
                <a href="#" id="myBtn">
                    <i class="fas fa-pause-circle"></i>
                </a>
            </div>

        </section>
        <!--end video section-->


        <!--start slider section-->
        <section class="mrg-top-xg mrg-btm-xg allWords">
            <div class="myContainer position-relative common-slider-btn">
                <div class="swiper-container home-first-slider ">
                    <div class="swiper-wrapper">
                        <?php
                        $slide_args = array(
                            'post_type' => 'slider',
                            "order" => 'ASC',
                            "orderby" => "menu_order ID",
                        );

                        $slide = get_posts($slide_args);

                        foreach ($slide as $slide) {
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'full');
                            $det = $slide->post_title;
                            $letter = $det[0][0];
                            ?>
                            <div class="swiper-slide">
                                <div class="imgWrapper">
                                    <img src="<?= $image[0]; ?>" alt="<?= $slide->post_title; ?>">
                                </div>
                                <div class="emptyDiv">
                                    <div class="absoluteDiv">
                                        <p class="mainColor aperturaRegular singlechar text-uppercase"><?= $letter; ?></p>
                                        <div class="mainColorBg around-Words clearfix">
                                            <p class="head white letter-4 text-uppercase after"><?= $slide->post_title; ?></p>
                                            <p class="white desc letter-4 letterContent f-normal"><?= $slide->post_content; ?></p>
                                            <a href="<?= get_page_link(17); ?>#<?= $slide->post_title; ?>">
                                                <button class="font-12 letter-4 commonReadMore">
                                                    READ MORE
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev allWordsbtn f-12 mainColor letter-4">PREVIOUS</div>
                <div class="position-absolute f-12 sep lightGray">/</div>
                <div class="swiper-button-next allWordsbtn f-12 mainColor letter-4">NEXT</div>
            </div>
        </section>
        <!--END Slider section-->

        <section class="home-unique d-flex position-relative">
            <div class="grayBg align-self-center">
                <div class="myContainer">
                    <div class="uniqueWrapper">
                        <?php
                        $title = get_post_meta($post->ID, 'title', true);

                        $title1 = explode('/', $title);

                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                        ?>
                        <p class="text-uppercase f-lg mainColor uniqueTitle"><?= $title = get_post_meta($post->ID, 'title', true); ?></p>
                        <div class="smallHr mainColorBg d-none d-md-block"></div>
                        <div class="toChangePos clearfix">
                            <p class="letter-4 twoLines"><?= get_post_meta($post->ID, 'text', true); ?></p>
                            <a href="story#Unique"><button class="f-12 letter-4 p-3 commonReadMore no mainColor">
                                READ MORE
                            </button></a>
                        </div>
                        <div class="uniqueImg centerImg-md">
                            <img src="<?= $image[0]; ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
    endwhile;
endif;
?>.
    <!--start project slider -->
    <section class="mrg-hr-130 projectWrapper fixProject">
        <div class="myContainer  common-slider-btn position-relative">
            <div>
                <p class="mainColor text-uppercase f-big letter-4">OUR PROJECTS</p>
            </div>
            <?php
            $project_args = array(
                'post_type' => 'projects',
                "order" => 'DESC',
            );
            $projects = get_posts($project_args);
            ?>
            <div class="flexConatiner projects-slider">
                <div class="flexslider">
                    <ul class="slides">
                        <?php
                        foreach ($projects as $project) {
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($project->ID), 'project-image')[0];
                            $logo = get_post_meta($project->ID, 'logo')[0]['guid'];
                            ?>
                            <li class="position-relative">
                                <div class="imgContainer">
                                    <img src="<?= $image; ?>" alt="">
                                </div>
                                <!-- new From Dalia -->
                                <div class="mainColorBg commonDiv position-absolute">
                                    <div class="projectLogo align-self-end">
                                        <div>
                                            <div class="logoImage" >
                                                <img src="<?= $logo; ?>" alt="">
                                            </div>
                                        </div>
                                        <div class="text-center align-self-end margin">
                                            <a href="<?= get_post_permalink($project->ID) ?>"
                                               class="aperturaRegular d-inline-block moreProject" rel="noreferrer">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- new From Dalia -->
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <script>
        $('.flexslider').flexslider({
            animation: "slide",
            slideshow: false,
        });
        $('.flex-next').prepend('<span class="mr-3 lightGray">/</span>');

        /*$('#test').click(function () {

            $(document).bind('keydown', 'shift+m');
        })*/
    </script>

    <!--end project slider -->
<?php get_footer(); ?>