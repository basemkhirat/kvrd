<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KVRD</title>
    <link rel="stylesheet" href="<?= get_template_directory_uri() . '/asset/css/pace-theme-center-simple.css'; ?>">
    <link rel="icon" type="image/x-icon" href="<?= get_template_directory_uri() . '/asset/images/logo.png'; ?>" />
    <?php wp_head(); ?>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/pace.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/jquery-3.3.1.min.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/bootstrap.min.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/bootstrap-datepicker.js'; ?>"></script>

    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/swiper.min.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/main.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/jquery.flexslider-min.js'; ?>"></script>
    <script type="text/javascript"src="<?= get_template_directory_uri() . '/asset/js/version4.js'; ?>"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCdbNGZNxXWApXH7VKPXkfE2tSqV4tYZ9o"
            async="" defer="defer"></script>

    <script>
        Pace.once('hide', function () {
            $('.loading').css('display', 'none');
        })
    </script>
</head>
<body>


<div class="loading">
    <div class="loadingContainer">
        <p>
            <span class="one">K</span>
            <span class="two">V</span><br>
            <span class="three">R</span>
            <span class="four">D</span>
        </p>
    </div>
</div>

<header>
    <?php

    $facebook = get_post_meta(21, 'facebook', true);
    $instagram = get_post_meta(21, 'instagram', true);
    $twitter = get_post_meta(21, 'twitter', true);
    $Linkedin = get_post_meta(21, 'linkedin', true);

    ?>
    <div class="top">
        <div class="myContainer clearfix position-relative">
            <a href="<?= home_url(); ?>" class="logoContainer">
                <img src="<?= get_template_directory_uri() . '/asset/images/logo.png'; ?>" alt="Logo"
                     class="float-left logo">
            </a>
            <div class="float-right">
                <a href="javascript:(void(0))" class="float-left navIcon">
                    <i class="fas fa-bars mainColor f-32"></i>
                    <i class="fas fa-times f-20 mainColor mainHover closeSubMenu"></i>
                </a>
                <div class="socialIcons pr-3 toHide">
                    <a href="<?= $Linkedin ?>" class="">
                        <i class="fab fa-linkedin"></i>
                    </a>
                    <a href="<?= $instagram ?>" class="">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="<?= $twitter ?>">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="<?= $facebook ?>" class="">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                </div>
                <div class="chat float-right">
                    <a href="<?= $facebook; ?>" class="mainColor">
                        <i class="fas fa-comment mr-1"></i>
                        LIVE CHAT
                    </a>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="bottom">
        <div class="myContainer clearfix position-relative">
            <a href="javascript:(void(0))" class="float-left navIcon">
                <i class="fas fa-bars mainColor f-32"></i>
                <i class="fas fa-times f-20 mainColor mainHover closeSubMenu"></i>
            </a>
            <?php
            $args = array(
                'menu' => 'main Menu',
                'container' => 'ul',
                'items_wrap' => '<ul class="float-left d-in-large mainUl">%3$s</ul>',

            );
            wp_nav_menu($args);

            ?>
            <?php get_search_form(); ?>

        </div>
    </div>

</header>
<div class="responsiveMenu whiteBg position-fixed">
    <div class="top clearfix">
        <div class="myContainer">
            <div class="socialIcons pr-3 toHide float-left">
                <a href="https://www.linkedin.com" class="" rel="noreferrer">
                    <i class="fab fa-linkedin"></i>
                </a>
                <a href="https://www.instagram.com/" class="" rel="noreferrer">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="https://twitter.com/" rel="noreferrer">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="https://www.facebook.com/KVRD.egy/ " class="" rel="noreferrer">
                    <i class="fab fa-facebook-square"></i>
                </a>
            </div>
            <div class="chat float-right">
                <a href="https://www.facebook.com/KVRD.egy/ " class="mainColor" rel="noreferrer">
                    <i class="fas fa-comment mr-1"></i>
                    LIVE CHAT
                </a>
            </div>
        </div>
    </div>
    <div class="form">
        <div class="myContainer">
            <?php get_template_part('searchform-2'); ?>
        </div>
    </div>
    <div class="menu">
        <div class="myContainer">
            <div>
                <?php
                $args = array(
                    'menu' => 'side Menu',
                    'container' => 'ul',
                    'items_wrap' => '<ul >%3$s</ul>',

                );
                wp_nav_menu($args);

                ?>

            </div>
        </div>
    </div>
</div>

<div class="alert alert-success text-center" style="display: none; position: fixed;z-index: 100000;">
    <strong>Success!</strong> This alert box could indicate a successful or positive action.
</div>
<div id="alert_danger" class="wrongEmail text-center"
     style="display: none; position: fixed;z-index: 100000; min-width: 29%">

</div>
