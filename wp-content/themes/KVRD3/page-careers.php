<?php get_header(); ?>

<?php // Show the selected careers content.
if (have_posts()) :

    $allow = wp_count_posts('careers')->publish;
    ?>

    <section
            class="ourProject forFixed">
        <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover"
             class="firstSection mb-0">
        </div>
        <div class="myContainer">
            <div class="mainColorBg pageTitle">
                <p class="white f-36 letter-4 text-center"><?= (($allow != 1) ? 'Careers' : 'Careers'); ?></p>
            </div>
        </div>
    </section>
    <?php if ($allow != 0): ?>
    <section class="all-career">
        <div class="myContainer">
            <div class="row">
                <?php
                $postsPerPage = 9;
                $careers_args = array(
                    'post_type' => 'careers',
                    'posts_per_page' => $postsPerPage,
                    "order" => 'ASC',
                );
                $careers = get_posts($careers_args);
                $count = wp_count_posts('careers')->publish;
                foreach ($careers as $career):
                    $excerpt = get_post_meta($career->ID, 'career_excerpt');
                    ?>
                    <div class="col-md-6 col-xl-4">
                        <div class="singleCarer">
                            <p class="f-big2 aperturaRegular letter-4 after"><?= $career->post_title; ?></p>
                            <p class="aperturaRegular twoLines letter-4 explain f-normal"><?= $excerpt[0]; ?>.</p>
                            <a href="<?= get_post_permalink($career->ID); ?>" class="aperturaRegular mainColor f-12 commonReadMore">
                                Apply now
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php if ($count >= 9) { ?>
                <button id="more" class="commonButton white mainColorBg mx-auto mt-20 border-0 moreCarers">
                    More
                </button>
            <?php } ?>
        </div>
    </section>
<?php else: ?>
    <section>
        <div class="myContainer">
            <p class="mainColor vacancies f-lg">
                NO OPEN VACANCIES
            </p>
        </div>
    </section>
<?php endif; ?>
    <?php
    while (have_posts()) : the_post();
    endwhile;
endif;
?>

    <script>
        var ppp = 9;
        var pageNumber = 1;

        $('#more').click(function () {
            pageNumber++;
            var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=career';

            $.ajax({
                type: "POST",
                dataType: "json",
                url: ajaxurl,
                data: str,
                success: function (data) {
                    console.log(data);
                    $('.all-career .myContainer .row').append(data.out);
                    if (data.t == false) {
                        $('#more').css("display", "none");
                    }
                }
            });
        });
    </script>

<?php get_footer(); ?>