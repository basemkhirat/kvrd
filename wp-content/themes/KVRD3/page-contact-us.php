<?php get_header(); ?>

<?php // Show the selected contactus content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover"
                 class="firstSection mb-0">
            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white f-36 letter-4 text-center">Contact Us</p>
                </div>
            </div>
        </section>

        <div class="myContainer">
            <p class="f-big mainColor quickContact">
                MAKE QUICK CONTACT
            </p>
        </div>

        <section class="map position-relative">
            <div style="height: 100%">
                <?php gmwd_map(1, 1); ?>
            </div>
            <?php

            $location = get_post_meta($id, 'location');
            $phone = get_post_meta($id, 'phone');

            ?>
            <div class="">
                <div class="overMap mainColorBg contactMap">
                    <div class="singleData">
                        <p class="f-big after">
                            Our location
                        </p>
                        <p class="f-normal ">
                            <?= get_post_meta($post->ID, 'location', true); ?>
                        </p>
                    </div>

                    <div class="singleData">
                        <p class="f-big after">
                            Talk With Us
                        </p>
                        <p class="f-normal">
                            <?= get_post_meta($post->ID, 'phone', true); ?>
                        </p>
                    </div>

                    <div class="singleData">
                        <p class="f-big after">
                            Send Your Words
                        </p>
                        <p class="f-normal">
                            <?= get_post_meta($post->ID, 'e-mail', true); ?>

                        </p>
                    </div>
                </div>
            </div>
        </section>


        <section class="inquiry">
            <div class="myContainer">
                <div class="applicationForm">
                    <form action="" id="prop_form">
                        <div class="personalInfo contactRelative">
                            <p class="f-big mainColor">SEND YOU MESSAGE</p>
                            <div class="row position-relative">
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input name="name" id="first_name" type="text" maxlength="10" placeholder="First Name*"  class="form-control aperturaMedium" pattern="^[A-Za-z -]+$" >
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="last_name" type="text" placeholder="Last Name" maxlength="10"
                                               class="form-control aperturaMedium" required pattern="[A-Za-z]+">
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="email" type="email" maxlength="50" placeholder="E-mail*"
                                               class="form-control aperturaMedium" required>
                                    </div>
                                </div>


                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="number" type="text" maxlength="15" placeholder="Contact Number"
                                               class="form-control aperturaMedium" required pattern="[0-9]+">
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-8">
                                    <div class="form-group textArea">
                                        <textarea name="" id="message" rows="7" class="f-12 form-control aperturaMedium"
                                                  placeholder="Your Message"></textarea>
                                    </div>
                                </div>
                                <div class="position position-absolute">
                                    <button id="message_Form" type="submit" class="mainColorBg white border-0 f-12">Submit
                                    </button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </section>

        <!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>-->
        <script type="text/javascript">
            $(function () {

                $('#prop_form').submit(function (e) {
                    e.preventDefault();
                    var first_name = $("#first_name").val();
                    var last_name = $("#last_name").val();
                    var email = $("#email").val();
                    var number = $("#number").val();
                    var message = $("#message").val();

                    var errors = [];

                    if (email == '') {
                        errors.push("email its required");
                        $("#email").parent().find('i').remove();
                        $("#email").css("border", "1px solid #d62222");
                        $("#email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                    } else {
                        $("#email").parent().find('i').remove();
                        var atpos = email.indexOf("@");
                        var dotpos = email.lastIndexOf(".");
                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
                            errors.push('Not a valid e-mail address!');
                            $("#email").parent().find('i').remove();
                            $("#email").css("border", "1px solid #d62222");
                            $("#email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                        } else {
                            $("#email").css("border", "1px solid #1EB52A");
                            $("#email").parent().find('i').remove();
                            $("#email").parent().append("<i class='fas fa-check-circle' style='font-size:18px;color:green'></i>");
                        }
                    }

                    if (number == '') {
                        errors.push('number its required!');
                        $("#number").parent().find('i').remove();
                        $("#number").css("border", "1px solid #d62222");
                        $("#number").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                    } else {
                        $("#number").parent().find('i').remove();
                        if (number.length < 8 || number.length > 15) {
                            errors.push('Invalid number format!');
                            $("#number").parent().find('i').remove();
                            $("#number").css("border", "1px solid #d62222");
                            $("#number").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                        } else if (isNaN(number) == true) {
                            errors.push('invalid number only contain number!');
                            $("#number").parent().find('i').remove();
                            $("#number").css("border", "1px solid #d62222");
                            $("#number").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                        }
                        else {
                            $("#number").css("border", "1px solid #1EB52A");
                            $("#number").parent().find('i').remove();
                            $("#number").parent().append("<i class='fas fa-check-circle' style='font-size:18px;color:green'></i>");
                        }
                    }

                    if (message == '') {
                        errors.push("message its empty");
                        $("#message").parent().find('i').remove();
                        $("#message").css("border", "1px solid #d62222");
                        $("#message").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                    } else {
                        $("#message").css("border", "1px solid #1EB52A");
                        $("#message").parent().find('i').remove();
                        $("#message").parent().append("<i class='fas fa-check-circle' style='font-size:18px;color:green'></i>");
                    }

                    if (first_name == '') {
                        errors.push("please type your name");
                        $("#first_name").parent().find('i').remove();
                        $("#first_name").css("border", "1px solid #d62222");
                        $("#first_name").parent().append("<i class='fa fa-exclamation-circle' style='font-size:18px;color:red'></i>");
                    } else {
                        $("#first_name").css("border", "1px solid #1EB52A");
                        $("#first_name").parent().find('i').remove();
                        $("#first_name").parent().append("<i class='fas fa-check-circle' style='font-size:18px;color:green'></i>");
                    }

                    if (errors != '') {
                        $('#alert_danger').empty();

                        for (i = 0; i < errors.length; i++) {
                            // console.log(errors[i]);

                            $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                            $("#danger" + i).append('<strong>Error!</strong> ' + errors[i]);

                        }

                        $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                            $("#alert_danger").slideUp(9000);
                        });
                    }

                    if (errors == '') {
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: ajaxurl,
                            cache: false,
                            data: {
                                "action": "message2",
                                email: email,
                                first_name: first_name,
                                last_name: last_name,
                                number: number,
                                message: message
                            },
                            success: function (data) {
                                // console.log(data[0].error);
                                error = data[0].error;
                                success = data[0].success;

                                if (data[0].error != '') {
                                    // $('#alert_danger').css('display', 'block');
                                    $('#alert_danger').empty();

                                    for (i = 0; i < data.length; i++) {
                                        // console.log(data[i].error);

                                        $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                                        $("#danger" + i).append('<strong>Error!</strong> ' + data[i].error);

                                    }

                                    $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $("#alert_danger").slideUp(9000);
                                    });

                                } else {
                                    $('.form-group i').remove()
                                    $('.form-group input').css('border', '1px solid rgba(0, 0, 0, 0.1)')
                                    $(".alert-success").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $(".alert-success").slideUp(9000);
                                    });
                                    $(".alert").html('<strong>Success!</strong> ' + success);

                                    $("#first_name").val('');
                                    $("#last_name").val('');
                                    $("#email").val('');
                                    $("#number").val('');
                                    $("#message").val('');
                                }
                            }
                        });
                    }
                });


            });
        </script>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>