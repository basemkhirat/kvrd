<?php get_header(); ?>

<?php // Show the selected media content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>

        <section class="forFixed mediaCenter">
            <div style="background-image: url(<?= get_template_directory_uri() . '/asset/images/mediacenter.png'; ?>); background-size: cover; background-position: right">

            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white f-36 letter-4 text-center">Media Center</p>
                </div>
            </div>
        </section>

        <?php
        $postsPerPage = 4;
        $query_images_args = array(
            'post_type' => 'gallery',
            'posts_per_page' => $postsPerPage,
            "order" => 'ASC',
        );
        $media = get_posts($query_images_args);
        foreach ($media as $media) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($media->ID), 'full');
            $images [] = $image[0];
            $gallery_id[] = $media->ID;

        }
        $count = wp_count_posts('gallery')->publish;
        ?>

        <section class="centerGallery mrg-50">
            <div class="myContainer ajaxGallery">
                <p class="sectionTitle f-big after letter-4">
                    PHOTO GALLERY
                </p>
                <?php if($count != 0){?>
                <div class="row">
                    <?php for ($i = 0; $i < 4; $i++) { ?>
                        <div class="col-md-4 singleGalley galleryHover">
                            <div class="centerImg position-relative sel_cat" onclick="test(<?= $gallery_id[$i]; ?>)">
                                <a href="javascript:void(0)"><img src="<?= $images[$i]; ?>" alt="" class="glalleryImg"></a>
                                <div class="mainColorBg centerDetails">
                        <span class="f-big float-left letter-4">
                            <?= get_the_title($gallery_id[$i]) ?>
                        </span>
                                    <span class="f-normal float-right">
                            <?= sizeof(get_post_meta($gallery_id[$i], 'upload')); ?> Photos
                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 singleGalley">
                            <div class="subGallery galleryHover">
                                <div class="centerImg sel_cat" onclick="test(<?= $gallery_id[++$i]; ?>)">
                                    <a href="javascript:void(0)"><img src="<?= $images[$i]; ?>" alt=""
                                                                      class="glalleryImg"></a>
                                </div>
                                <div class="mainColorBg centerDetails">
                        <span class="f-big float-left letter-4">
                            <?= get_the_title($gallery_id[$i]) ?>
                        </span>
                        <span class="f-normal float-right">
                            <?= sizeof(get_post_meta($gallery_id[$i], 'upload')); ?> Photos
                        </span>
                                </div>
                            </div>
                            <div class="subGallery galleryHover">
                                <div class="centerImg sel_cat" onclick="test(<?= $gallery_id[++$i]; ?>)">
                                    <a href="javascript:void(0)"><img src="<?= $images[$i]; ?>" alt=""
                                                                      class="glalleryImg"></a>
                                </div>
                                <div class="mainColorBg centerDetails">
                        <span class="f-big float-left letter-4">
                            <?= get_the_title($gallery_id[$i]) ?>
                        </span>
                         <span class="f-normal float-right">
                            <?= sizeof(get_post_meta($gallery_id[$i], 'upload')); ?> Photos
                        </span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4 singleGalley galleryHover">
                            <div class="centerImg position-relative sel_cat" onclick="test(<?= $gallery_id[++$i]; ?>)">
                                <a href="javascript:void(0)"><img src="<?= $images[$i]; ?>" alt="" class="glalleryImg"></a>
                                <div class="mainColorBg centerDetails">
                    <span class="f-big float-left letter-4">
                        <?= get_the_title($gallery_id[$i]) ?>
                    </span>
                                    <span class="f-normal float-right">
                        <?= $no_of_photo = sizeof(get_post_meta($gallery_id[$i], 'upload ')); ?> Photos
                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php }else{ ?>
                    <div>
                        <p class="f-lg mainColor">No AVAILABLE ALBUMS</p>

                    </div>
                <?php }?>
            </div>
            <?php if ($count > 4) { ?>
            <button id="more" class="commonButton white mainColorBg border-0 mx-auto aperturaRegular">
                More
            </button>
            <?php }?>
            <div class="galleryPopUp projectWrapper">
                <div class="myContainer common-slider-btn position-relative clearfix">
                    <a href="javascript:void(0)" class="float-right mainColor f-20 closeGallery">
                        <i class="fas fa-times"></i>
                    </a>
                    <div class="flexConatiner projects-slider">
                        <div class="popup-slider flexslider">
                            <div class="flex-viewport">
                                <ul class="slides" id="swiper-photo">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php

        $query_video_args = array(
            'post_type' => 'test',
            'posts_per_page' => -1,
        );
        $video_media = get_posts($query_video_args);

        ?>

        <section class="centerVideo projectWrapper mrg-50">
            <div class="myContainer common-slider-btn position-relative">
                <p class="sectionTitle f-big after letter-4">
                    VIDEO GALLERY
                </p>
                <div class="flexConatiner projects-slider">
                    <div class="videosSlider flexslider">
                        <div class="flex-viewport">
                            <ul class="slides">
                                <?php foreach ($video_media as $videos) {
                                    $id = $videos->ID;
                                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'video-image');
                                    $video = get_post_meta($id, 'media');
//                                echo $video[0]['guid'];
                                    ?>
                                    <li class="position-relative galleryHover">
                                        <div class="imgContainer">
                                            <img src="<?= $image[0]; ?>" alt="">
                                            <!--<video controls="" autoplay class="videoCart">
                                                <source src="<?/*= $video[0]['guid'] */?>">
                                            </video>-->
                                        </div>
                                        <div class="position-absolute subVideo">
                                            <a href="javascript:void(0)" class="f-big">
                                                <!--<i class="far fa-play-circle"></i>-->
                                                <i class="far fa-play-circle video-play" data-url="<?= $video[0]['guid']; ?>"></i>
                                            </a>
                                        </div>
                                        <div class="mainColorBg centerDetails">
                                <span class="f-big float-left">
                                    <?= get_the_title($id) ?>
                                </span>
                                            <span class="f-normal float-right">
                                    02:00
                                </span>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pop-up commonPop">
                <a href="javascript:void(0)" class="float-right">
                    <i class="fa fa-bars f-22 fa-times mainColor closeMyPopup"></i>
                </a>
                <div class="h-100 d-flex justify-content-center mt-5 align-items-center">
                    <video mute class="myVideo" id="myVideo2">
                        <source src="" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
            </div>
        </section>
        <?php

        $query_press_args = array(
            'post_type' => 'press',
            'posts_per_page' => -1,
        );
        $press = get_posts($query_press_args);


        ?>

        <section class="release mrg-50" style="margin-bottom: 100px">
            <div class="myContainer position-relative">
                <p class="sectionTitle f-big after letter-4">
                    PRESS RELEASE
                </p>
                <div class="swiper-container pressRelease">
                    <div class="swiper-wrapper">
                        <?php foreach ($press as $press) {

                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($press->ID), 'press-slider');
                            $no_of_photo = sizeof(get_post_meta($press->ID, 'gallery'));
                            $date = get_the_date('F Y', $press->ID);
                            ?>
                            <div class="swiper-slide">
                                <div class="imgContainer centerImg">
                                    <img src="<?= $image[0] ?>" alt="">
                                </div>
                                <div class="releaseContainer clearfix">
                                    <p class="releaseMainText mainColor"><?= $press->post_title; ?></p>
                                    <p class="f-normal description textColor"><?= $press->post_excerpt; ?>.</p>
                                    <a href="<?= get_post_permalink($press->ID) ?>">
                                        <button class="f-12 letter-4 commonReadMore mainColor">
                                            READ MORE
                                        </button>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="swiper-button-prev forRelease">PREVIOUS
                    <span class="ml-2">/</span>
                </div>
                <div class="swiper-button-next forRelease">NEXT
                </div>
            </div>
        </section>
    <?php
    endwhile;
endif;
?>
    <script type="text/javascript">
        function test(img_id){

            var gallery_id = img_id;

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: ajaxurl,
                cache: false,
                data: {"action": "gallery", gallery_id: gallery_id},
                success: function (data) {

                    $("#swiper-photo").empty();

                    loop(data);

                    $('.popup-slider').flexslider({
                        animation: "slide",
                        slideshow: false,
                        minItems: 1,
                        start: function () {
                            var totalSlides = $('.popup-slider .flex-control-nav li').length;
                            $('.galleryPopUp .total').text(totalSlides);
                            $('.galleryPopUp .flex-prev').append('<i style="margin-left: 8px; color: #99999A">/</i>')
                        },
                        after: function () {
                            var currentIndex = $('.popup-slider li:has(.flex-active)').index('.flex-control-nav li') + 1;
                            $('.galleryPopUp .current').text(currentIndex + ' / ')
                        }
                    });
                    $('.galleryPopUp').css({
                        'opacity': 1,
                        'z-index': 999
                    });

                }
            });

            function loop(data) {
                var all = data;

                jQuery.each(all, function (i, val) {
                    $("#swiper-photo").prepend(
                        '<li class="position-relative galleryHover"><div class="imgContainer"><img src=" ' + val.url + '" alt=""></div><div class="mainColorBg centerDetails"><span class="f-big float-left letter-4">' + val.title + '</span><p class="f-normal float-right"><span class="current">1 / </span><span class="total mr-1"> </span> Photos</p></div></li>')
                });

            }

        }
    </script>

    <script>
        var ppp = 4;
        var pageNumber = 1;

        $('#more').click(function () {
            pageNumber++;
            var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_photo_ajax';

            $.ajax({
                type: "POST",
                dataType: "json",
                url: ajaxurl,
                data: str,
                success: function (data) {

                    $('.centerGallery .myContainer.ajaxGallery').append(data.out);
                    if (data.t == false) {
                        $('#more').css("display", "none");
                    }
                }
            });

        });
    </script>

<?php get_footer(); ?>