<?php get_header(); ?>

<?php // Show the selected Mission content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/mission.png'; ?>'); background-size: cover"
                 class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white letter-4 f-36 text-center" style="width: 400px!important;">Mission & Vision</p>
                </div>

            </div>
        </section>

        <section class="letterDescription missionContainer">
            <div class="myContainer">
                <div class="imgContainer forMobile" style="height: auto; width: auto">
                    <img src="<?= get_template_directory_uri() . '/asset/images/mission-single2.png'; ?>" alt="">
                </div>
                <div class="imgContainer centerImg forDesktop">
                    <img src="<?= get_template_directory_uri() . '/asset/images/mission-single.png'; ?>" alt="">
                </div>
                <div class="mission">
                    <div class="mainColorBg pageTitle">
                        <p class="f-big after white letter-4"><?= get_post_meta($post->ID, 'title1', true); ?></p>
                        <span class="f-normal white forDesktop letter-4"><?= get_post_meta($post->ID, 'description1', true); ?></span>
                    </div>
                </div>
                <span class="f-normal textColor forMobile letter-4"><?= get_post_meta($post->ID, 'description1', true); ?></span>

            </div>
        </section>

        <section class="letterDescription missionContainer" style="margin-bottom: 100px">
            <div class="myContainer">
                <div class="imgContainer forMobile" style="height: auto; width: auto">
                    <img src="<?= get_template_directory_uri() . '/asset/images/vision2.png'; ?>" alt="">
                </div>
                <div class="imgContainer centerImg forDesktop">
                    <img src="<?= get_template_directory_uri() . '/asset/images/v%20letter.png'; ?>" alt="">
                </div>
                <div class="mission">
                    <div class="mainColorBg pageTitle">
                        <p class="f-big after white letter-4"><?= get_post_meta($post->ID, 'title2', true); ?></p>
                        <span class="f-normal white forDesktop letter-4"><?= get_post_meta($post->ID, 'description2', true); ?></span>
                    </div>
                </div>

                <span class="f-normal textColor forMobile letter-4"><?= get_post_meta($post->ID, 'description2', true); ?></span>


            </div>
        </section>
    <?php
    endwhile;
endif; ?>

<?php get_footer(); ?>