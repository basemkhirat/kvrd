<?php get_header('2'); ?>

<?php // Show the selected value content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>

        <section
                class="forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset2/images/value1.jpg'; ?>'); background-size: cover"
                 class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="f-36 white letter-4 text-center">Our values</p>

                </div>
            </div>
        </section>

        <section class="value-2">
            <div class="myContainer">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/excellence.png'; ?>" alt=""
                                     class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'excellence', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'excellence-description', true); ?></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/innovation.png'; ?>" alt=""
                                     class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'innovation', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'innovation-description', true); ?></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/reliability.png'; ?>"
                                     alt="" class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'reliability', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'reliability-description', true); ?></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/Empower.png'; ?>" alt=""
                                     class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'empowerment', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'empowerment-description', true); ?></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/research.png'; ?>" alt=""
                                     class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'research-focused', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'research-description', true); ?></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">

                        <div class="valueContent">
                            <div class="image">
                                <img src="<?= get_template_directory_uri() . '/asset/images/focus.png'; ?>"
                                     alt="" class="d-block">
                            </div>
                            <p class="mainColor after f-big"><?= get_post_meta($post->ID, 'future-focused', true); ?></p>
                            <p class="desc"><?= get_post_meta($post->ID, 'future-description', true); ?></p>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>