<?php get_header(); ?>

<?php // Show the selected project content.
if (have_posts()) :
    while (have_posts()) : the_post();

        $terms = get_terms('project-type');
        ?>
        <section
                class="ourProject forFixed mrg-btm-xg">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white letter-4 f-36 text-center" style="width: 400px!important;">Our
                        Projects</p>
                </div>
            </div>
        </section>

        <section class="allProjects">
            <div class="myContainer">
                <?php
                $postsPerPage = 2;
                $project_args = array(
                    'post_type' => 'projects',
                    'posts_per_page' => $postsPerPage,
                    "order" => 'ASC',
                    'post_status'      => 'publish'
                );
                $projects = get_posts($project_args);
                $count = wp_count_posts('projects')->publish;
                foreach ($projects as $project) {
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($project->ID), 'full')[0];
                    $logo = get_post_meta($project->ID, 'logo')[0]['guid'];
                    $excerpt = get_post_meta($project->ID, 'project_excerpt');
                    ?>
                    <div class="singleProject clearfix position-relative mrg-btm-lg">
                        <div class="col-md-12 image centerImg-md p-0">
                            <img src="<?= $image; ?>" alt="">
                        </div>
                        <div class="mainColorBg version4">
                            <div class="projectLogo m-auto">
                                <a href="<?= get_post_permalink($project->ID) ?>"> <img src="<?= $logo; ?>" alt=""></a>
                            </div>

                        </div>
                    </div>
                <?php } ?>

            </div>
            <?php if ($count > 2) { ?>
                <button id="more" class="commonButton white mainColorBg border-0 mx-auto">
                    More
                </button><?php } ?>
        </section>
        <script>
            var ppp = 2;
            var pageNumber = 1;

            $('#more').click(function () {
                pageNumber++;
                var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_ajax';

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: ajaxurl,
                    data: str,
                    success: function (data) {

                        $('.allProjects .myContainer').append(data.out);
                        if (data.t == false) {
                            $('#more').css("display", "none");
                        }
                    }
                });
            });
        </script>


    <?php
    endwhile;
endif;

?>


<?php get_footer(); ?>