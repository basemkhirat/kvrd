<?php get_header(); ?>

<?php // Show the selected property content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/carrers.png'; ?>'); background-size: cover"
                 class="firstSection mb-0">
            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white f-36 letter-4 text-center">Property Inquiries</p>
                </div>
            </div>
        </section>

        <section class="inquiry">
            <div class="myContainer">
                <p class="f-big mainColor">PLEASE WRITE YOUR DETAILS HERE</p>
                <div class="applicationForm">
                    <form action="" id="prop_form">
                        <div class="personalInfo">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="name" placeholder="Full Name" maxlength="30" type="text" class="form-control f-input" id="name"
                                               required pattern="^[A-Za-z -]+$">
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="prop_email" placeholder="Email" maxlength="50" type="email" class="form-control f-input"
                                               id="email" required>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <input id="mobile" placeholder="Mobile" min="1" max="5" maxlength="15" type="text" class="form-control f-input" id="mobile" required>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <select name="" id="owner" class="form-control f-input">
                                            <option value="">KVRD Customer</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>

                                <?php
                                $project_args = array(
                                    'post_type' => 'projects',
                                    "order" => 'ASC',
                                );
                                $projects = get_posts($project_args);


                                ?>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <select name="" id="project" class="form-control f-input ">
                                            <option value="">Project of Interest</option>
                                            <?php foreach ($projects as $project) {
                                                $term_id = wp_get_post_terms($project->ID, array('project-type'))[0]->term_id;
                                                ?>

                                                <option term="<?= $term_id ?>"
                                                        value="<?= $project->post_title; ?>"><?= $project->post_title; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <select name="" id="type" class="form-control f-input">
                                            <option value="">Type</option>

                                        </select>
                                    </div>
                                </div>
                                <div id="show-bedroom" class="col-md-6 col-lg-4" style="display: none">
                                    <div class="form-group">
                                        <input type="text" id="bedroom" placeholder="Bedroom Number" class="form-control f-input">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="position">
                            <div class="myContainer text-center">
                                <button id="prop_submit" type="submit" class="mainColorBg white border-0 f-12">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
        <!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>-->
        <script type="text/javascript">

            $('#project').change(function () {
                var type = $('#project').find(":selected").attr("term");
                $('#type').empty();
                if (type == 6) {
                    // Offices, Clininics, Retail Space
                    $('#type').append('<option value="">Select Type</option>\n');
                    $('#type').append('<option value="Offices">Offices</option>');
                    $('#type').append('<option value="Clininics">Clininics</option>');
                    $('#type').append('<option value="Retail Space">Retail Space</option>');
                    $('#show-bedroom').css("display", "none");
                }
                else if (type == 7) {
                    // apartment, villa, twin house, town house, duplex, studio
                    $('#type').append('<option value="">Select Type</option>\n');
                    $('#type').append('<option value="apartment">apartment</option>');
                    $('#type').append('<option value="villa">villa</option>');
                    $('#type').append('<option value="twin house">twin house</option>');
                    $('#type').append('<option value="town house">town house</option>');
                    $('#type').append('<option value="duplex">duplex</option>');
                    $('#type').append('<option value="studio">studio</option>');
                    $('#show-bedroom').css("display", "block");
                }
            });

            $(function () {

                $('#prop_form').submit(function (e) {
                    e.preventDefault();
                    var name = $("#name").val();
                    var email = $("#prop_email").val();
                    var mobile = $("#mobile").val();
                    var owner = $("#owner").val();
                    var project = $("#project").val();
                    var type = $("#type").val();
                    var bedroom = $("#bedroom").val();

                    var errors = [];

                    if (email == '') {
                        errors.push("email its required");
                        $("#prop_email").parent().find('i').remove();
                        $("#prop_email").css("border", "1px solid #d62222");
                        $("#prop_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    } else {
                        var atpos = email.indexOf("@");
                        var dotpos = email.lastIndexOf(".");
                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
                            errors.push('Not a valid e-mail address!');
                            $("#prop_email").parent().find('i').remove();
                            $("#prop_email").css("border", "1px solid #d62222");
                            $("#prop_email").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                        }
                    }


                    if (mobile == '') {
                        errors.push('number its required!');
                        $("#mobile").parent().find('i').remove();
                        $("#mobile").css("border", "1px solid #d62222");
                        $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                    } else {

                        if (isNaN(mobile) == true) {
                            errors.push('invalid number only contain number!');
                            $("#mobile").parent().find('i').remove();
                            $("#mobile").css("border", "1px solid #d62222");
                            $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                        } else {
                            if (mobile.length < 8 || mobile.length > 15) {
                                errors.push('valid number must be between 8 and 15 numbers !');
                                $("#mobile").parent().find('i').remove();
                                $("#mobile").css("border", "1px solid #d62222");
                                $("#mobile").parent().append("<i class='fa fa-exclamation-circle' style='font-size:24px;color:red'></i>");
                            }
                        }

                    }

                    if (errors != '') {
                        $('#alert_danger').empty();

                        for (i = 0; i < errors.length; i++) {
                            // console.log(errors[i]);

                            $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                            $("#danger" + i).append('<strong>Error!</strong> ' + errors[i]);

                        }

                        $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                            $("#alert_danger").slideUp(9000);
                        });
                    }

                    if (errors == '') {

                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: ajaxurl,
                            cache: false,
                            data: {
                                "action": "property",
                                email: email,
                                name: name,
                                mobile: mobile,
                                owner: owner,
                                project: project,
                                type: type,
                                bedroom: bedroom
                            },
                            success: function (data) {
                                // console.log(data);
                                error = data[0].error;
                                success = data[0].success;

                                if (data[0].error != '') {
                                    $('#alert_danger').empty();

                                    for (i = 0; i < data.length; i++) {
                                        // console.log(data[i].error);

                                        $('#alert_danger').append('<div id="danger' + i + '" class="alert alert-danger text-center"></div>');
                                        $("#danger" + i).append('<strong>Error!</strong> ' + data[i].error);

                                    }

                                    $("#alert_danger").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $("#alert_danger").slideUp(9000);
                                    });


                                } else {

                                    $(".alert-success").fadeTo(9000, 9000).slideUp(9000, function () {
                                        $(".alert-success").slideUp(9000);
                                    });
                                    $(".alert").html('<strong>Success!</strong> ' + success);

                                    $("#name").val('');
                                    $("#prop_email").val('');
                                    $("#mobile").val('');
                                    $("#owner").val('');
                                    $("#project").val('');
                                    $("#type").val('');
                                    $("#bedroom").val('');

                                }
                            }
                        });
                    }
                });


            });
        </script>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>