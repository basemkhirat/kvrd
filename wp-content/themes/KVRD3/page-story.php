<?php get_header('2'); ?>

<?php // Show the selected storypage content.
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <section
                class="ourProject forFixed">
            <div style="background-image: url('<?= get_template_directory_uri() . '/asset/images/story2.png'; ?>'); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="mainColorBg pageTitle">
                    <p class="white letter-4 f-36 text-center" style="width: 400px!important;">Our
                        Story</p>
                </div>
            </div>
        </section>
        <?php
        $img_id = get_post_meta($post->ID, 'photo1')[0];
        $back_image1 = wp_get_attachment_url($img_id);
        ?>
        <section class="home-unique d-flex position-relative" id="Unique">
            <div class="grayBg align-self-center">
                <div class="myContainer">
                    <div class="uniqueWrapper">
                        <p class="text-uppercase f-lg mainColor uniqueTitle"><?= get_post_meta($post->ID, 'title2', true); ?></p>
                        <div class="smallHr mainColorBg d-none d-md-block"></div>
                        <div class="toChangePos clearfix">
                            <p class="letter-4 "><?= get_post_meta($post->ID, 'description2', true); ?></p>
<!--                            <button class="f-12 letter-4 p-3 commonReadMore mainColor">-->
<!--                                READ MORE-->
<!--                            </button>-->
                        </div>
                        <div class="uniqueImg centerImg-md">
                            <img src="<?=$back_image1;?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $img_id2 = get_post_meta($post->ID, 'photo_2')[0];
        $back_image2 = wp_get_attachment_url($img_id2);
        ?>

        <section class="projectPage p-ver-40">
            <div class="myContainer">
                <div class="clearfix d-flex flex-column d-md-block">
                    <p class="mainColor rightBorder text-uppercase f-big"><?= get_post_meta($post->ID, 'title3', true); ?> </p>
<!--                    <div class="smallHr mainColorBg"></div>-->
                    <p class=" target order-3 leftBorder f-normal"><?= get_post_meta($post->ID, 'description3', true); ?></p>
                    <div class="projectImage centerImg-md">
                        <img src="<?= $back_image2 ?>" alt="">
                    </div>
                </div>
            </div>
        </section>
    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>