<?php get_header(); ?>

<?php // Show the selected beyond content.
if (have_posts()) :
    while (have_posts()) : the_post();

?>


        <section
                class="ourProject forFixed">
            <div style="background-image: url(<?= get_template_directory_uri() . '/asset/images/story2.png' ?>); background-size: cover" class="firstSection mb-0">

            </div>
            <div class="myContainer">
                <div class="d-flex flex-wrap">
                    <div class="mainColorBg pageTitle align-content-center">
                        <p class="white letter-4 f-36" style="width: 400px!important;">The Essence Beyond</p>
                        <span class="f-md">K V R D Name Inspiration</span>
                    </div>
                    <div class="wordDescription">
                        <p class="f-normal textColor letter-4">
                            Originaly the name KVRD was not just a name that outlines the company, it also
                        </p>
                        <div class="d-flex justify-content-between flex-wrap sepWords">
                            <p class="f-normal mainColor">
                                <span class="font-weight-bold">K</span>
                                <span> Knowledge</span>
                            </p>
                            <p class="f-normal mainColor">
                                <span class="font-weight-bold">V</span>
                                <span> Vision</span>
                            </p>
                            <p class="f-normal mainColor">
                                <span class="font-weight-bold">R</span>
                                <span> Research</span>
                            </p>
                            <p class="f-normal mainColor">
                                <span class="font-weight-bold">D</span>
                                <span> Development</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            $slide_args = array(
                'post_type' => 'slider',
                "order" => 'ASC',
                "orderby" => "menu_order ID",
            );

            $slide = get_posts($slide_args);

            $i = 0;
            $title = [];
        foreach ($slide as $slide) {
            $title[$i] = $slide->post_title;
            $i++;
        }
        ?>
        <?php
        $det = get_post_meta($post->ID, 'title1', true);
        $letter1 = $det[0];
        ?>
        <section class="letterDescription" id="knowledge">
            <div class="myContainer">
                <div class="imgContainer centerImg">
                    <img src="<?= get_template_directory_uri() . '/asset/images/k%20letter.png'?>" alt="">
                </div>
                <div class="text">
                    <div class="">
                        <span class="mainColor"><?=$letter1?></span>
                        <div class="mainColorBg pageTitle">
                            <p class="white letter-4 f-big align-self-center after"><?=$det;?></p>
                        </div>
                    </div>
                    <p class="f-normal description letter-4 textColor"><?= get_post_meta($post->ID, 'description1', true); ?>.</p>
                </div>

            </div>
        </section>

        <?php
        $det = get_post_meta($post->ID, 'title2', true);
        $letter2 = $det[0];
        ?>

        <section class="letterDescription" id="Vision">
            <div class="myContainer">
                <div class="imgContainer centerImg">
                    <img src="<?= get_template_directory_uri() . '/asset/images/v%20letter.png'?>" alt="">
                </div>
                <div class="text">
                    <div class="">
                        <span class="mainColor" style="font-size: 84px; line-height: 1"><?=$letter2;?></span>
                        <div class="mainColorBg pageTitle">
                            <p class="white letter-4 f-big align-self-center after"><?=$det;?></p>
                        </div>
                    </div>
                    <p class="f-normal description letter-4 textColor"><?= get_post_meta($post->ID, 'description2', true); ?> </p>
                </div>

            </div>
        </section>

        <?php
        $det = get_post_meta($post->ID, 'title3', true);
        $letter3 = $det[0];
        ?>

        <section class="letterDescription" id="Research">
            <div class="myContainer">
                <div class="imgContainer centerImg">
                    <img src="<?= get_template_directory_uri() . '/asset/images/r%20letter.png';?>" alt="">
                </div>
                <div class="text">
                    <div class="">
                        <span class="mainColor">R</span>
                        <div class="mainColorBg pageTitle">
                            <p class="white letter-4 f-big align-self-center after"><?=$det;?></p>
                        </div>
                    </div>
                    <p class="f-normal description letter-4 textColor"><?= get_post_meta($post->ID, 'description3', true); ?></p>
                </div>

            </div>
        </section>

        <?php
        $det = get_post_meta($post->ID, 'title4', true);
        $letter4 = $det[0];
        ?>

        <section class="letterDescription last" id="Development">
            <div class="myContainer">
                <div class="imgContainer centerImg">
                    <img src="<?= get_template_directory_uri() . '/asset/images/d%20letter.png';?>" alt="">
                </div>
                <div class="text">
                    <div class="">
                        <span class="mainColor"><?=$letter4;?></span>
                        <div class="mainColorBg pageTitle">
                            <p class="white letter-4 f-big align-self-center after"><?=$det;?></p>
                        </div>
                    </div>
                    <p class="f-normal description letter-4 textColor"><?= get_post_meta($post->ID, 'description4', true); ?></p>
                </div>

            </div>
        </section>

<?php
    endwhile;
endif;
?>


<?php get_footer(); ?>