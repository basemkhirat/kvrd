<?php get_header(); ?>

    <section
            class="ourProject forFixed">
        <div style="background-image: url(<?= get_template_directory_uri() . '/asset/images/story2.png' ?>); background-size: cover"
             class="firstSection mb-0">

        </div>
        <div class="myContainer">
            <div class="d-flex flex-wrap">
                <div class="mainColorBg pageTitle align-content-center">
                    <p class="white letter-4 f-36" style="width: 400px!important;">Search Result For :</p>
                    <span class="f-md"><?php the_search_query(); ?></span>
                </div>
            </div>
        </div>
    </section>

<?php

if (have_posts()) :
    ?>
    <section class="searchRes">
        <div class="myContainer">
            <div class="row">
    <?php
    while (have_posts()) : the_post();
        get_template_part('content', get_post_format());
        if (has_post_thumbnail()) {
            $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'search');
            $image = $img[0];
        }
        ?>

        <div class="col-lg-6 <?= ((has_post_thumbnail()) ? 'withImg' : '') ?>">
            <div class="d-flex searchContainer">
                <?php if (has_post_thumbnail()): ?>
                    <div class="imgContainer centerImg">
                        <img src="<?= $image; ?>" alt="">
                    </div>
                <?php endif; ?>
                <div class="data">
                    <a href="<?= get_permalink(); ?>" class="f-md mainColor"><?= $post->post_title; ?></a>
                    <span class="f-normal twoLines hideText">
                            <?php
                            if($post->post_type == "careers")
                                echo get_post_meta($post->ID,'career_excerpt',true);
                            else
                                echo  $post->post_content;
                            ?>
                           </span>
                </div>
            </div>
        </div>

    <?php

    endwhile; ?>
    </div>
    </div>
    </section>
    <?php
    echo paginate_links();

else : ?>

    <section class="searchRes">
        <div class="myContainer">
            <div class="row">
                <div class="col-lg-12 d-flex text-center f-big mainColor">
                    <p class="data">No search result found</p>
                </div>
            </div>
        </div>
    </section>
<?php
endif;

get_footer();

?>