<form action="<?=home_url('/'); ?>" method="get" class="position-relative">
    <input type="text" name="s" placeholder="Search" class="form-control" required>
    <button type="submit" class="mainColor">
        <i class="fas fa-search"></i>
    </button>
</form>
