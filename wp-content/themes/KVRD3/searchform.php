
    <form action="<?=home_url('/'); ?>" method="get" class="position-relative searchForm">
        <input type="text" name="s" placeholder="Search" required maxlength="25">
        <button class="position-absolute" name="submit">
            <i class="fas fa-search mainColor"></i>
        </button>
        <span class="danger">Please enter a search word</span>

    </form>
    <a href="javascript:void(0)" class="mainColor mainHover searchIcon toHide pl-3">
        <i class="fas fa-search"></i>
    </a>
</div>

    <script>
        $('input').focus(function () {
            if(document.getElementsByClassName('danger')[0].style.display == 'block') {
                $('.danger').css('display', 'none');
                $(this).val('');``
            }
        });
        $('[name="submit"]').on('click',function (e) {
            str = $('[name="s"]').val();
            if(!str || str.length === 0 || /^\s*$/.test(str))
            {
                e.preventDefault();
                $('.danger').css('display', 'block')
            }
        })
    </script>