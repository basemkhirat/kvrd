<?php get_header();
if (have_posts()):
    while (have_posts()) : the_post();
        $id = get_the_ID();
        for ($i = 0; $i < sizeof(get_post_meta($id, 'gallery')); $i++) {

            $images [] = get_post_meta($id, 'gallery')[$i]['guid'];
            $lg_img[] = pods_image_url($images [$i], 'press', 0, '', true);
        }

        $no_of_photo = sizeof(get_post_meta($id, 'gallery'));
        $date = get_the_date('F Y', $press->ID);

        $back_image_web = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'press-thumbnail')[0];
        $back_image_mob = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'press-thumbnail-mob')[0];

        $id = get_the_author_meta('ID');
        ?>

        <section class="forFixed articelDetails">
            <div class="myContainer">
                <div class="projectImage">
                    <img src="<?= $back_image_web; ?>" alt="<?php the_title(); ?>">

                    <img class="mobile" style="display: none" src="<?= $back_image_mob; ?>" alt="<?php the_title(); ?>">
                </div>
                <p class="articleTitle mainColor"><?php the_title(); ?></p>
                <div class="f-normal articleParagraph"><?php the_content(); ?></div>
            </div>
        </section>

    <?php
    endwhile;
endif;
?>


<?php get_footer(); ?>